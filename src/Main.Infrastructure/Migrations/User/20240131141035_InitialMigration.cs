﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Main.Infrastructure.Migrations.User
{
    /// <inheritdoc />
    public partial class InitialMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "userSchema");

            migrationBuilder.CreateTable(
                name: "users",
                schema: "userSchema",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    name = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    username = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    password = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    clearance = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_users", x => x.Id);
                });

            migrationBuilder.InsertData(
                schema: "userSchema",
                table: "users",
                columns: new[] { "Id", "clearance", "name", "password", "username" },
                values: new object[,]
                {
                    { new Guid("619bd1f1-db45-4ce9-b29f-c938a88ce905"), 1, "Teste", "c3k/DJbf1Kh18VJyeTtM569IRwEvLn++/Nu/xgkckgA=", "Teste" },
                    { new Guid("fe017578-6f6a-41f0-aa02-70516b4d7531"), 0, "Admin", "8zXcC64eSFiBFKHZeTgnmUD9mEBHcKZZrOa4UbG+wrc=", "Admin" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "users",
                schema: "userSchema");
        }
    }
}
