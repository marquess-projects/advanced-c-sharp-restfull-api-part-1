﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Main.Infrastructure.Migrations.Vehicle
{
    /// <inheritdoc />
    public partial class InitialMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "vehicleSchema");

            migrationBuilder.CreateTable(
                name: "brands",
                schema: "vehicleSchema",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    name = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_brands", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "models",
                schema: "vehicleSchema",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    name = table.Column<string>(type: "text", nullable: false),
                    brand_id = table.Column<Guid>(type: "uuid", nullable: false),
                    enable = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_models", x => x.id);
                    table.ForeignKey(
                        name: "FK_models_brands_brand_id",
                        column: x => x.brand_id,
                        principalSchema: "vehicleSchema",
                        principalTable: "brands",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "vehicles",
                schema: "vehicleSchema",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    name = table.Column<string>(type: "text", nullable: false),
                    model_id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_vehicles", x => x.id);
                    table.ForeignKey(
                        name: "FK_vehicles_models_model_id",
                        column: x => x.model_id,
                        principalSchema: "vehicleSchema",
                        principalTable: "models",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "vehicleSchema",
                table: "brands",
                columns: new[] { "id", "name" },
                values: new object[,]
                {
                    { new Guid("0532cf75-ca95-4974-87f7-9fcca3cfabce"), "Ford" },
                    { new Guid("631ff755-b5b0-41b6-9282-c252bb08b24f"), "Chevrolet" }
                });

            migrationBuilder.InsertData(
                schema: "vehicleSchema",
                table: "models",
                columns: new[] { "id", "brand_id", "enable", "name" },
                values: new object[,]
                {
                    { new Guid("0ad70ce4-21b5-4ec0-bc6a-a4788a3b1467"), new Guid("0532cf75-ca95-4974-87f7-9fcca3cfabce"), true, "GT 500" },
                    { new Guid("9288c145-6165-4874-a4af-109d1e4c983c"), new Guid("631ff755-b5b0-41b6-9282-c252bb08b24f"), true, "SS 396 L89" }
                });

            migrationBuilder.InsertData(
                schema: "vehicleSchema",
                table: "vehicles",
                columns: new[] { "id", "model_id", "name" },
                values: new object[,]
                {
                    { new Guid("65f89f42-9107-4945-947b-757767098fb1"), new Guid("9288c145-6165-4874-a4af-109d1e4c983c"), "Camaro" },
                    { new Guid("d026424a-c01d-4161-afb1-16d8b330d751"), new Guid("0ad70ce4-21b5-4ec0-bc6a-a4788a3b1467"), "Mustang" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_models_brand_id",
                schema: "vehicleSchema",
                table: "models",
                column: "brand_id");

            migrationBuilder.CreateIndex(
                name: "IX_vehicles_model_id",
                schema: "vehicleSchema",
                table: "vehicles",
                column: "model_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "vehicles",
                schema: "vehicleSchema");

            migrationBuilder.DropTable(
                name: "models",
                schema: "vehicleSchema");

            migrationBuilder.DropTable(
                name: "brands",
                schema: "vehicleSchema");
        }
    }
}
