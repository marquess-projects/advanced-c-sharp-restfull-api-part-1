using Main.Application.Contracts;
using Main.Infrastructure.Persistence.Contexts;
using Main.Infrastructure.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Main.Infrastructure.Persistence.Extensions
{
    public static class PersistenceServiceRegistration
    {
        public static IServiceCollection AddPersistenceServices(
            this IServiceCollection services, 
            IConfiguration configuration)
        {
            services.AddRepositoryServices();

            services.AddDbContexts(configuration);

            return services;
        }

        private static void AddRepositoryServices(
            this IServiceCollection services)
        {
            services.AddScoped<IVehicleRepository, VehicleRepository>();

            services.AddScoped<IUserRepository, UserRepository>();
        }

        private static void AddDbContexts(
            this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddDbContext<VehicleContext>(options =>
                options.UseNpgsql(
                    configuration.GetConnectionString("MainDatabaseConnection"),
                    x => x.MigrationsHistoryTable("__VehicleMigrationsHistory", "vehicleSchema")
                )
            );

            services.AddDbContext<UserContext>(options =>
                options.UseNpgsql(
                    configuration.GetConnectionString("MainDatabaseConnection"),
                    x => x.MigrationsHistoryTable("__UserMigrationsHistory", "userSchema")
                )
            );
        }
    }
}

