using Main.Application.Contracts;
using Main.Application.Features.Common;
using Microsoft.EntityFrameworkCore;
using Main.Infrastructure.Persistence.Contexts;
using Main.Core.Data;
using Main.Domain.Entities.VehicleEntity;
using Main.Domain.Entities.ModelEntity;
using Main.Domain.Entities.BrandEntity;

namespace Main.Infrastructure.Persistence.Repositories
{
    public class VehicleRepository(VehicleContext vehicleContext) 
        : IVehicleRepository
    {
        public  VehicleContext _vehicleContext = vehicleContext;

        public IUnitOfWork UnitOfWork => _vehicleContext;

        // ADD

        public void AddOne(Vehicle vehicle)
        {
            _vehicleContext.Add(vehicle);
        }

        public void AddOne(Model model)
        {
            _vehicleContext.Add(model);
        }

        public void AddOne(Brand brand)
        {
            _vehicleContext.Add(brand);
        }

        // REMOVE

        public void RemoveOne(Vehicle vehicle)
        {
            _vehicleContext.Remove(vehicle);
        }

        public void RemoveOne(Model model)
        {
            _vehicleContext.Remove(model);
        }

        public void RemoveOne(Brand brand)
        {
            _vehicleContext.Remove(brand);
        }

        // EXISTS

        public async Task<bool> BrandExistsAsync(Guid brandId)
        {
            return await _vehicleContext.Brands
                .AnyAsync(e => e.Id == brandId);
        }
        
        public async Task<bool> ModelExistsAsync(Guid modelId)
        {
            return await _vehicleContext.Models
                .AnyAsync(e => e.Id == modelId);
        }

        // HAS

        public async Task<bool> HasModelWithBrand(Guid brandId)
        {
            return await _vehicleContext.Models
                .AnyAsync(e => e.BrandId == brandId);
        }

        public async Task<bool> HasVehicleWithModel(Guid modelId)
        {
            return await _vehicleContext.Vehicles
                .AnyAsync(e => e.ModelId == modelId);
        }

        // GET

        public async Task<Vehicle?> GetOneVehicleAsync(Guid vehicleId)
        {            
            return await _vehicleContext.Vehicles
                .Include(e => e.Model)
                .ThenInclude(e => e.Brand)
                .FirstOrDefaultAsync(e => e.Id == vehicleId);
        }

        public async Task<(ICollection<Vehicle>, PaginationMetadata)> GetManyVehiclesAsync(
            int pageNumber, 
            int pageSize, 
            string searchQuery = "", 
            string vehicleName = "")
        {
            var collection = _vehicleContext.Vehicles
                .Include(e => e.Model)
                .ThenInclude(e => e.Brand)
                as IQueryable<Vehicle>;

            // Filtro
            if (string.IsNullOrWhiteSpace(vehicleName) == false)
            {
                vehicleName = vehicleName.Trim();

                collection = collection.Where(
                    e => e.Name == vehicleName
                );
            }

            // Pesquisa
            if (string.IsNullOrWhiteSpace(searchQuery) == false)
            {
                searchQuery = searchQuery.Trim();

                collection = collection.Where(
                    e => e.Name.Contains(searchQuery) 
                );
            }

            var totalItemCount = await collection.CountAsync();

            var paginationMetadata = new PaginationMetadata(
                totalItemCount, pageSize, pageNumber
            );

            var vehiclesToReturn = await collection
                .OrderBy(e => e.Id)
                .Skip(pageSize * (pageNumber - 1))
                .Take(pageSize)
                .ToListAsync();

            return (vehiclesToReturn, paginationMetadata);
        }

        public async Task<Brand?> GetOneBrandAsync(Guid brandId)
        {            
            return await _vehicleContext.Brands
                .FirstOrDefaultAsync(e => e.Id == brandId);
        }

        public async Task<(ICollection<Brand>, PaginationMetadata)> GetManyBrandsAsync(
            int pageNumber, 
            int pageSize, 
            string searchQuery = "", 
            string brandName = "")
        {
            var collection = _vehicleContext.Brands as IQueryable<Brand>;

            // Filtro
            if (string.IsNullOrWhiteSpace(brandName) == false)
            {
                brandName = brandName.Trim();

                collection = collection.Where(
                    e => e.Name == brandName
                );
            }

            // Pesquisa
            if (string.IsNullOrWhiteSpace(searchQuery) == false)
            {
                searchQuery = searchQuery.Trim();

                collection = collection.Where(
                    e => e.Name.Contains(searchQuery) 
                );
            }

            var totalItemCount = await collection.CountAsync();

            var paginationMetadata = new PaginationMetadata(
                totalItemCount, pageSize, pageNumber
            );

            var brandsToReturn = await collection
                .OrderBy(e => e.Id)
                .Skip(pageSize * (pageNumber - 1))
                .Take(pageSize)
                .ToListAsync();

            return (brandsToReturn, paginationMetadata);
        }

        public async Task<Model?> GetOneModelAsync(Guid modelId)
        {            
            return await _vehicleContext.Models
                .Include(e => e.Brand)
                .FirstOrDefaultAsync(e => e.Id == modelId);
        }

        public async Task<(ICollection<Model>, PaginationMetadata)> GetManyModelsAsync(
            int pageNumber, 
            int pageSize, 
            string searchQuery = "", 
            string modelName = "")
        {
            var collection = _vehicleContext
                .Models.Include(e => e.Brand) as IQueryable<Model>;

            // Filtro
            if (string.IsNullOrWhiteSpace(modelName) == false)
            {
                modelName = modelName.Trim();

                collection = collection.Where(
                    e => e.Name == modelName
                );
            }

            // Pesquisa
            if (string.IsNullOrWhiteSpace(searchQuery) == false)
            {
                searchQuery = searchQuery.Trim();

                collection = collection.Where(
                    e => e.Name.Contains(searchQuery) 
                );
            }

            var totalItemCount = await collection.CountAsync();

            var paginationMetadata = new PaginationMetadata(
                totalItemCount, pageSize, pageNumber
            );

            var modelsToReturn = await collection
                .OrderBy(e => e.Id)
                .Skip(pageSize * (pageNumber - 1))
                .Take(pageSize)
                .ToListAsync();

            return (modelsToReturn, paginationMetadata);
        }
    }
}