using Main.Application.Contracts;
using Main.Domain.Entities.UserEntity;
using Main.Infrastructure.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;

namespace Main.Infrastructure.Persistence.Repositories
{
    public class UserRepository(UserContext userContext) : IUserRepository
    {
        private readonly UserContext _userContext = userContext;

        public async Task<bool> SaveChanges()
        {
            return await _userContext.SaveChangesAsync() > 0;
        }

        public void AddOne(User user)
        {
            _userContext.Users.Add(user);
        }

        public void DeleteOne(User user)
        {
            _userContext.Users.Remove(user);
        }

        public async Task<ICollection<User>> GetAllAsync()
        {
            return await _userContext.Users.ToListAsync();
        }    

        public async Task<User?> GetOneByIdAsync(Guid userId)
        {
            return await _userContext.Users
                .FirstOrDefaultAsync(e => e.Id == userId);
        }

        public async Task<User?> GetOneByUsernameAsync(string username)
        {
            return await _userContext.Users
                .FirstOrDefaultAsync(u => u.Username == username);
        }    

        public async Task<bool> UsernameExistsAsync(string username)
        {
            return await _userContext.Users
                .AnyAsync(e => e.Username == username);
        }
    }
}