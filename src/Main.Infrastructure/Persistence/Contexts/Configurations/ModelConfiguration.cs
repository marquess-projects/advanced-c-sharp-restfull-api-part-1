using Main.Domain.Entities.ModelEntity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Main.Infrastructure.Persistence.Contexts.Configurations
{
    public class ModelConfiguration : IEntityTypeConfiguration<Model>
    {
        public void Configure(EntityTypeBuilder<Model> builder)
        {
            builder.ToTable("models", "vehicleSchema");

            builder
                .Property(e => e.Id)
                .HasColumnName("id")
                .IsRequired();
            
            builder
                .Property(e => e.Name)
                .HasColumnName("name")
                .IsRequired();

            builder
                .HasOne(e => e.Brand)
                .WithMany()
                .HasForeignKey(e => e.BrandId)
                .IsRequired();

            builder
                .Property(e => e.BrandId)
                .HasColumnName("brand_id");

            builder
                .Property(e => e.Enable)
                .HasColumnName("enable");
        }
    }
}