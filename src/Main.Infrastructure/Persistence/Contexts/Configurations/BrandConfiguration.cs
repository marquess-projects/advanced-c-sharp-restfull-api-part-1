using Main.Domain.Entities.BrandEntity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Main.Infrastructure.Persistence.Contexts.Configurations
{
    public class BrandConfiguration : IEntityTypeConfiguration<Brand>
    {
        public void Configure(EntityTypeBuilder<Brand> builder)
        {
            builder.ToTable("brands", "vehicleSchema");

            builder
                .Property(e => e.Id)
                .HasColumnName("id")
                .IsRequired();
            
            builder
                .Property(e => e.Name)
                .HasColumnName("name")
                .IsRequired();
        }
    }
}