using Main.Domain.Entities.VehicleEntity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Main.Infrastructure.Persistence.Contexts.Configurations
{
    public class VehicleConfiguration : IEntityTypeConfiguration<Vehicle>
    {
        public void Configure(EntityTypeBuilder<Vehicle> builder)
        {
            builder.ToTable("vehicles", "vehicleSchema");

            builder
                .Property(e => e.Id)
                .HasColumnName("id")
                .IsRequired();
            
            builder
                .Property(e => e.Name)
                .HasColumnName("name")
                .IsRequired();

            builder
                .HasOne(e => e.Model)
                .WithMany()
                .HasForeignKey(e => e.ModelId)
                .IsRequired();

            builder
                .Property(e => e.ModelId)
                .HasColumnName("model_id");

            /* images
            builder
                .HasMany(c => c.ChargeProducts)
                .WithMany(p => p.Charges)
                .UsingEntity<ChargesProducts>(
                    cs => cs.ToTable("ChargesProducts")
                );
            */
        }
    }
}