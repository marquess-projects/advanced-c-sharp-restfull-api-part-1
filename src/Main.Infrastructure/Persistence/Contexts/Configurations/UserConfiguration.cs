using Main.Domain.Entities.UserEntity;
using Main.Core.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Main.Infrastructure.Persistence.Contexts.Configurations
{
    public class UserConfiguration(HashPassword hashPassword)
        : IEntityTypeConfiguration<User>
    {
        private readonly HashPassword _hashPassword = hashPassword;

        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("users", "userSchema");

            builder
                .Property(e => e.Name)
                .HasColumnName("name")
                .HasMaxLength(100)
                .IsRequired();

            builder
                .Property(e => e.Username)
                .HasColumnName("username")
                .HasMaxLength(100)
                .IsRequired();

            builder
                .Property(e => e.Password)
                .HasColumnName("password")
                .HasMaxLength(100)
                .IsRequired();

            builder
                .Property(e => e.Clearance)
                .HasColumnName("clearance")
                .IsRequired();
        }

        public void SeedData(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(
                new User(
                    "Admin",
                    "Admin",
                    _hashPassword.From("123"),
                    Clearance.Admin
                ),
                new User(
                    "Teste",
                    "Teste",
                    _hashPassword.From("456"),
                    Clearance.ViewOnly
                )
            );
        }
    }
}