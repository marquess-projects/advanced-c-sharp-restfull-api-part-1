using Main.Core.Utils;
using Main.Domain.Entities.UserEntity;
using Main.Infrastructure.Persistence.Contexts.Configurations;
using Microsoft.EntityFrameworkCore;

namespace Main.Infrastructure.Persistence.Contexts
{
    public class UserContext(
        DbContextOptions<UserContext> options, 
        HashPassword hashPassword) 
        : DbContext(options)
    {
        private readonly HashPassword _hashPassword = hashPassword;
        
        public DbSet<User> Users { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var userConfiguration = new UserConfiguration(_hashPassword);

            modelBuilder.ApplyConfiguration(userConfiguration);

            userConfiguration.SeedData(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }
    }
}