using Main.Core.Data;
using Main.Domain.Entities.BrandEntity;
using Main.Domain.Entities.ModelEntity;
using Main.Domain.Entities.VehicleEntity;
using Main.Infrastructure.Persistence.Contexts.Configurations;
using Microsoft.EntityFrameworkCore;

namespace Main.Infrastructure.Persistence.Contexts
{
    public class VehicleContext(DbContextOptions<VehicleContext> options) 
        : DbContext(options), IUnitOfWork
    {
        public DbSet<Vehicle> Vehicles { get; set; } = null!;
        public DbSet<Model> Models { get; set; } = null!;
        public DbSet<Brand> Brands { get; set; } = null!;

        public async Task<bool> Commit()
        {
            return await base.SaveChangesAsync() > 0;
        }

        protected override void OnConfiguring(
            DbContextOptionsBuilder optionsBuilder) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new VehicleConfiguration());
            modelBuilder.ApplyConfiguration(new ModelConfiguration());
            modelBuilder.ApplyConfiguration(new BrandConfiguration());

            DevelopmentConfiguration(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

        private static void DevelopmentConfiguration(ModelBuilder modelBuilder)
        {
            var b1 = new Brand("Ford");
            var b2 = new Brand("Chevrolet");

            var m1 = new Model("GT 500", b1.Id);
            var m2 = new Model("SS 396 L89", b2.Id);

            var v1 = new Vehicle("Mustang", m1.Id);
            var v2 = new Vehicle("Camaro", m2.Id);

            modelBuilder.Entity<Brand>().HasData(b1, b2);
            modelBuilder.Entity<Model>().HasData(m1, m2);
            modelBuilder.Entity<Vehicle>().HasData(v1, v2);
        }
    }
}