using FluentValidation.Results;

namespace Main.Application.Features.Common
{
    public enum Error 
    { 
        ValidationProblem, 
        NotFoundProblem, 
        BadRequestProblem
    }

    public abstract class BaseResponse 
    {
        public IDictionary<string, string[]> Errors { get; private set; } 
            = new Dictionary<string, string[]>();

        public Error ErrorType { get; private set; }

        public bool IsValid 
        {
            get { return Errors.Count == 0; }
        }    

        public void AddErrors(
            ValidationResult validationResult, 
            Error errorType)
        {
            Errors = validationResult.ToDictionary();
            
            ErrorType = errorType;
        }
    }
}