using Main.Application.Features.Common;

namespace Main.Application.Features.Models.Queries.GetMany
{
    public class GetManyModelsResponse : BaseResponse
    {
        public ICollection<GetManyModelsDTO> Models {get;set;} = null!;

        public PaginationMetadata PaginationMetadata = null!;
    }
}