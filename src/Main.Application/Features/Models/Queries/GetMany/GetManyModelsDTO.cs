using Main.Application.Features.Brands.Queries.GetMany;

namespace Main.Application.Features.Models.Queries.GetMany 
{
    public class GetManyModelsDTO 
    {
        public Guid Id { get; init; }
        public string Name { get; init; } = null!;
        public bool Enable { get; init; }
        public GetManyBrandsDTO Brand { get; init; } = null!;
    }
}