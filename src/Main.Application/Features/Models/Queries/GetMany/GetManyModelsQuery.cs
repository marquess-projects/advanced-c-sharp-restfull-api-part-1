using MediatR;

namespace Main.Application.Features.Models.Queries.GetMany
{
    public class GetManyModelsQuery 
        : IRequest<GetManyModelsResponse>
    {
        public int PageNumber { get; private set; }
        public int PageSize { get; private  set; }
        public string SearchQuery { get; private  set; }
        public string ModelName { get; private  set; }

        public GetManyModelsQuery(
            int pageNumber,
            int pageSize,
            string searchQuery,
            string modelName)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchQuery = searchQuery;
            ModelName = modelName;
        }
    }
}