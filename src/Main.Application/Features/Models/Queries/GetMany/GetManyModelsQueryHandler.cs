using AutoMapper;
using Main.Application.Contracts;
using MediatR;

namespace Main.Application.Features.Models.Queries.GetMany
{
    public class GetManyModelsQueryHandler(
        IVehicleRepository vehicleRepository,
        IMapper mapper)
        : IRequestHandler<GetManyModelsQuery, GetManyModelsResponse>
    {
        private readonly IVehicleRepository _vehicleRepository = vehicleRepository;
        
        private readonly IMapper _mapper = mapper;

        public async Task<GetManyModelsResponse> Handle(
            GetManyModelsQuery getManyModelsQuery, 
            CancellationToken cancellationToken)
        {
            GetManyModelsResponse getManyModelsResponse = new();

            var (modelsEntities, paginationMetadata) = 
                await _vehicleRepository.GetManyModelsAsync(
                    getManyModelsQuery.PageNumber, 
                    getManyModelsQuery.PageSize,
                    getManyModelsQuery.SearchQuery, 
                    getManyModelsQuery.ModelName
                );

            getManyModelsResponse.PaginationMetadata = paginationMetadata;

            getManyModelsResponse.Models = 
                _mapper.Map<ICollection<GetManyModelsDTO>>(modelsEntities);

            return getManyModelsResponse;
        }
    }
}