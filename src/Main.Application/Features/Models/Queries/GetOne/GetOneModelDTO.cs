using Main.Application.Features.Brands.Queries.GetOne;

namespace Main.Application.Features.Models.Queries.GetOne
{
    public class GetOneModelDTO
    {
        public Guid Id { get; init; }
        public string Name { get; init; } = null!;
        public bool Enable { get; init; }
        public GetOneBrandDTO Brand { get; init; } = null!;
    }
}