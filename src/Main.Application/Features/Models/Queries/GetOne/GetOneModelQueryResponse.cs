using Main.Application.Features.Common;

namespace Main.Application.Features.Models.Queries.GetOne;

public class GetOneModelQueryResponse : BaseResponse
{
    public GetOneModelDTO Model { get; set; } = default!;
}