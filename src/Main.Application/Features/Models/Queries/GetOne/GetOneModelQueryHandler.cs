using AutoMapper;
using FluentValidation.Results;
using Main.Application.Contracts;
using Main.Application.Features.Common;
using MediatR;

namespace Main.Application.Features.Models.Queries.GetOne;

public class GetOneModelQueryHandler(
    IVehicleRepository vehicleRepository,
    IMapper mapper)
    : IRequestHandler<GetOneModelQuery, GetOneModelQueryResponse>
{
    private readonly IVehicleRepository _vehicleRepository = vehicleRepository;

    private readonly IMapper _mapper = mapper;

    public async Task<GetOneModelQueryResponse> Handle(
        GetOneModelQuery getOneModelQuery, 
        CancellationToken cancellationToken)
    {
        var response = new GetOneModelQueryResponse();

        var validator = new GetOneModelQueryValidator();

        var validationResult = await validator
            .ValidateAsync(getOneModelQuery, cancellationToken);

        if (validationResult.IsValid == false)
        {
            response.AddErrors(validationResult, Error.ValidationProblem);

            return response;
        }

        var modelEntity = await _vehicleRepository
            .GetOneModelAsync(getOneModelQuery.Id);

        if (modelEntity is null)
        {
            validationResult.Errors.Add(
                new ValidationFailure(
                    "Model", "The model with the provided ID does not exist."
                )
            );

            response.AddErrors(validationResult, Error.NotFoundProblem);

            return response;
        }

        response.Model = _mapper.Map<GetOneModelDTO>(modelEntity);

        return response;
    }
}