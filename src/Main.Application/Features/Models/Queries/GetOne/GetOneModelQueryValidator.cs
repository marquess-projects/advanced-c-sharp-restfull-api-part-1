using FluentValidation;

namespace Main.Application.Features.Models.Queries.GetOne;

public class GetOneModelQueryValidator 
    : AbstractValidator<GetOneModelQuery>
{
    public GetOneModelQueryValidator()
    {
        RuleFor(e => e.Id)
            .NotEmpty()
            .WithMessage("{PropertyName} is required.");
    }
}