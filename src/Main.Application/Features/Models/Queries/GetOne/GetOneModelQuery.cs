using MediatR;

namespace Main.Application.Features.Models.Queries.GetOne;

public record GetOneModelQuery(Guid Id) 
    : IRequest<GetOneModelQueryResponse> { }