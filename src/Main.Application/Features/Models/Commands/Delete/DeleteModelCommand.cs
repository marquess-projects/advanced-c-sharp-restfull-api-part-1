using MediatR;

namespace Main.Application.Features.Models.Commands.Delete
{
    public record DeleteModelCommand(Guid Id) 
        : IRequest<DeleteModelCommandResponse>
    {
    }
}