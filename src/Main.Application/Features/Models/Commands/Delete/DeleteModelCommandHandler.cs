using AutoMapper;
using Main.Application.Contracts;
using Main.Application.Features.Common;
using FluentValidation.Results;
using MediatR;

namespace Main.Application.Features.Models.Commands.Delete
{
    public class DeleteModelCommandHandler(
        IVehicleRepository vehicleRepository,
        IMapper mapper)
        : IRequestHandler<DeleteModelCommand, DeleteModelCommandResponse>
    {
        private readonly IVehicleRepository _vehicleRepository = vehicleRepository;

        private readonly IMapper _mapper = mapper;

        public async Task<DeleteModelCommandResponse> Handle(
            DeleteModelCommand deleteModelCommand,
            CancellationToken cancellationToken)
        {
            var validator = new DeleteModelCommandValidator();

            var validationResult = await validator.ValidateAsync(
                deleteModelCommand, cancellationToken
            );

            if (validationResult.IsValid == false)
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.ValidationProblem
                );
            }

            var modelEntity = await _vehicleRepository
                .GetOneModelAsync(deleteModelCommand.Id);

            if (modelEntity is null)
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.ValidationProblem,
                    "The model with the provided ID does not exist."
                );
            }

            if (await _vehicleRepository.HasVehicleWithModel(deleteModelCommand.Id))
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.ValidationProblem,
                    "Exclusion is not possible because there are vehicles" + 
                    "with the model provided."
                );
            }

            _vehicleRepository.RemoveOne(modelEntity);

            await _vehicleRepository.UnitOfWork.Commit();

            return new DeleteModelCommandResponse();;
        }

        private static DeleteModelCommandResponse ErrorsResponse(
            ValidationResult validationResult,
            Error errorType,
            string? error = null)
        {
            var response = new DeleteModelCommandResponse();

            if (error != null) {
                validationResult.Errors.Add(
                    new ValidationFailure(
                        "Model", error
                    )
                );
            }
            
            response.AddErrors(validationResult, errorType);

            return response;
        }
    }
}