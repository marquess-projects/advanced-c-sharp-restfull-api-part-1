using FluentValidation;

namespace Main.Application.Features.Models.Commands.Delete
{
    public class DeleteModelCommandValidator
        : AbstractValidator<DeleteModelCommand>
    {
        public DeleteModelCommandValidator()
        {
            RuleFor(e => e.Id)
                .NotEmpty()
                .WithMessage("{PropertyName} is rquired");
        }
    }
}