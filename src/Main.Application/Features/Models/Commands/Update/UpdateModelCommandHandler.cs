using AutoMapper;
using Main.Application.Contracts;
using Main.Application.Features.Common;
using FluentValidation.Results;
using MediatR;

namespace Main.Application.Features.Models.Commands.Update
{
    public class UpdateModelCommandHandler(
        IVehicleRepository vehicleRepository,
        IMapper mapper)
        : IRequestHandler<UpdateModelCommand, UpdateModelCommandResponse>
    {
        private readonly IVehicleRepository _vehicleRepository = vehicleRepository;

        private readonly IMapper _mapper = mapper;

        public async Task<UpdateModelCommandResponse> Handle(
            UpdateModelCommand updateModelCommand,
            CancellationToken cancellationToken)
        {
            var validator = new UpdateModelCommandValidator();

            var validationResult = await validator.ValidateAsync(
                updateModelCommand, cancellationToken
            );

            if (validationResult.IsValid == false)
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.ValidationProblem
                );
            }

            var modelEntity = await _vehicleRepository.GetOneModelAsync(
                updateModelCommand.Id
            );

            if (modelEntity is null)
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.NotFoundProblem,
                    "The model with the provided ID does not exist."
                );
            }

            var brandId = updateModelCommand.BrandId;

            if (await _vehicleRepository.BrandExistsAsync(brandId) == false)
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.NotFoundProblem,
                    "The brand with the provided ID does not exist."
                );
            }

            modelEntity.Update(
                updateModelCommand.Name,
                brandId,
                updateModelCommand.Enable
            );

            await _vehicleRepository.UnitOfWork.Commit();

            return new UpdateModelCommandResponse();
        }

        private static UpdateModelCommandResponse ErrorsResponse(
            ValidationResult validationResult,
            Error errorType,
            string? error = null)
        {
            var response = new UpdateModelCommandResponse();

            if (error != null) {
                validationResult.Errors.Add(
                    new ValidationFailure(
                        "Model", error
                    )
                );
            }
            
            response.AddErrors(validationResult, errorType);

            return response;
        }
    }
}