using FluentValidation;

namespace Main.Application.Features.Models.Commands.Update
{
    public class UpdateModelCommandValidator
        : AbstractValidator<UpdateModelCommand>
    {
        public UpdateModelCommandValidator()
        {
            RuleFor(e => e.Id)
                .NotEmpty()
                .WithMessage("{PropertyName} is rquired");
            
            RuleFor(e => e.BrandId)
                .NotEmpty()
                .WithMessage("{PropertyName} is required.");
        }
    }
}