using MediatR;

namespace Main.Application.Features.Models.Commands.Update
{
    public record UpdateModelCommand(Guid Id)
        : IRequest<UpdateModelCommandResponse>
    {
        public string Name { get; init; } = null!;
        public Guid BrandId { get; init; }
        public bool Enable { get; set; } = true;
    }
}