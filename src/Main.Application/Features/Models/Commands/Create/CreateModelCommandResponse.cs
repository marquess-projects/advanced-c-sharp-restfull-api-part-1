using Main.Application.Features.Common;

namespace Main.Application.Features.Models.Commands.Create
{
    public class CreateModelCommandResponse : BaseResponse
    {
        public CreateModelDTO Model { get; set; } = default!;
    }
}