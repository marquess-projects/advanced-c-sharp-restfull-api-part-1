using Main.Application.Features.Brands.Commands.Create;

namespace Main.Application.Features.Models.Commands.Create
{
    public class CreateModelDTO
    {
        public Guid Id { get; init; }
        public string Name { get; init; } = null!;
        public CreateBrandDTO Brand { get; set; } = null!;
    }
}