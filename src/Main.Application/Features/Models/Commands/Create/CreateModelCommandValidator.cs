using FluentValidation;

namespace Main.Application.Features.Models.Commands.Create
{
    public class CreateModelCommandValidator
        : AbstractValidator<CreateModelCommand>
    {
        public CreateModelCommandValidator()
        {
            RuleFor(e => e.Name)
                .NotEmpty()
                .WithMessage("{PropertyName} is required.");

            RuleFor(e => e.BrandId)
                .NotEmpty()
                .WithMessage("{PropertyName} is required.");
        }
    }
}