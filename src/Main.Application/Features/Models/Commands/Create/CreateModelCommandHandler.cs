using AutoMapper;
using FluentValidation.Results;
using Main.Application.Contracts;
using Main.Application.Features.Common;
using Main.Domain.Entities.ModelEntity;
using Main.Domain.Entities.VehicleEntity;
using MediatR;

namespace Main.Application.Features.Models.Commands.Create
{
    public class CreateModelCommandHandler(
        IVehicleRepository vehicleRepository,
        IMapper mapper)
        : IRequestHandler<CreateModelCommand, CreateModelCommandResponse>
    {
        private readonly IVehicleRepository _vehicleRepository = vehicleRepository;

        private readonly IMapper _mapper = mapper;

        public async Task<CreateModelCommandResponse> Handle(
            CreateModelCommand createModelCommand,
            CancellationToken cancellationToken)
        {
            var validator = new CreateModelCommandValidator();

            var validationResult = await validator.ValidateAsync(
                createModelCommand, cancellationToken
            );

            if (validationResult.IsValid == false)
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.ValidationProblem
                );
            }

            var brandEntity = await _vehicleRepository
                .GetOneBrandAsync(createModelCommand.BrandId);

            if (brandEntity is null)
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.NotFoundProblem,
                    "The brand with the provided ID does not exist."
                );
            }

            var newModel = new Model (
                createModelCommand.Name,
                brandEntity
            );

            _vehicleRepository.AddOne(newModel);

            await _vehicleRepository.UnitOfWork.Commit();

            return new CreateModelCommandResponse()
            {
                Model = _mapper.Map<CreateModelDTO>(newModel)
            };
        }

        private static CreateModelCommandResponse ErrorsResponse(
            ValidationResult validationResult,
            Error errorType,
            string? error = null)
        {
            var response = new CreateModelCommandResponse();

            if (error != null) {
                validationResult.Errors.Add(
                    new ValidationFailure(
                        "Model", error
                    )
                );
            }
            
            response.AddErrors(validationResult, errorType);

            return response;
        }
    }
}