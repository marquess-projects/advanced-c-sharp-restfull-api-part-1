using Main.Domain.Entities.BrandEntity;
using MediatR;

namespace Main.Application.Features.Models.Commands.Create
{
    public record CreateModelCommand
        : IRequest<CreateModelCommandResponse>
    {
        public string Name { get; init; } = null!;
        public Guid BrandId { get; init; }
    }
}