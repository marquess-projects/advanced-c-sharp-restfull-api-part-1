using FluentValidation;

namespace Main.Application.Features.Vehicles.Queries.GetOne;

public class GetOneVehicleQueryValidator 
    : AbstractValidator<GetOneVehicleQuery>
{
    public GetOneVehicleQueryValidator()
    {
        RuleFor(e => e.Id)
            .NotEmpty()
            .WithMessage("{PropertyName} is required.");
    }
}