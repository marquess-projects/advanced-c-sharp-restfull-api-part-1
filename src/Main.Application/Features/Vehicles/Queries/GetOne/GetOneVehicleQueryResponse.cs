using Main.Application.Features.Common;

namespace Main.Application.Features.Vehicles.Queries.GetOne;

public class GetOneVehicleQueryResponse : BaseResponse
{
    public GetOneVehicleDTO Vehicle { get; set; } = default!;
}