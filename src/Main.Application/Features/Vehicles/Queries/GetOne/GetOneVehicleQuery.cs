using MediatR;

namespace Main.Application.Features.Vehicles.Queries.GetOne;

public record GetOneVehicleQuery(Guid Id) 
    : IRequest<GetOneVehicleQueryResponse> { }