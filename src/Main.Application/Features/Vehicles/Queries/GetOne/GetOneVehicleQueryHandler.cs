using AutoMapper;
using FluentValidation.Results;
using Main.Application.Contracts;
using Main.Application.Features.Common;
using MediatR;

namespace Main.Application.Features.Vehicles.Queries.GetOne;

public class GetOneVehicleQueryHandler(
    IVehicleRepository vehicleRepository,
    IMapper mapper)
    : IRequestHandler<GetOneVehicleQuery, GetOneVehicleQueryResponse>
{
    private readonly IVehicleRepository _vehicleRepository = vehicleRepository;

    private readonly IMapper _mapper = mapper;

    public async Task<GetOneVehicleQueryResponse> Handle(
        GetOneVehicleQuery getOneVehicleQuery, 
        CancellationToken cancellationToken)
    {
        var response = new GetOneVehicleQueryResponse();

        var validator = new GetOneVehicleQueryValidator();

        var validationResult = await validator
            .ValidateAsync(getOneVehicleQuery, cancellationToken);

        if (validationResult.IsValid == false)
        {
            response.AddErrors(validationResult, Error.ValidationProblem);

            return response;
        }

        var vehicleEntity = await _vehicleRepository
            .GetOneVehicleAsync(getOneVehicleQuery.Id);

        if (vehicleEntity is null)
        {
            validationResult.Errors.Add(
                new ValidationFailure(
                    "Vehicle", "The vehicle with the provided ID does not exist."
                )
            );

            response.AddErrors(validationResult, Error.NotFoundProblem);

            return response;
        }

        response.Vehicle = _mapper.Map<GetOneVehicleDTO>(vehicleEntity);

        return response;
    }
}