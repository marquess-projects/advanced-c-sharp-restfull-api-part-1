using Main.Application.Features.Models.Queries.GetOne;

namespace Main.Application.Features.Vehicles.Queries.GetOne
{
    public class GetOneVehicleDTO
    {
        public Guid Id { get; init; }
        public string Name { get; init; } = null!;
        public GetOneModelDTO Model { get; init; } = null!;
    }
}