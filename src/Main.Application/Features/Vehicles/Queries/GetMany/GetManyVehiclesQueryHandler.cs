using AutoMapper;
using Main.Application.Contracts;
using MediatR;

namespace Main.Application.Features.Vehicles.Queries.GetMany
{
    public class GetManyVehiclesQueryHandler(
        IVehicleRepository vehicleRepository,
        IMapper mapper)
        : IRequestHandler<GetManyVehiclesQuery, GetManyVehiclesResponse>
    {
        private readonly IVehicleRepository _vehicleRepository = vehicleRepository;
        private readonly IMapper _mapper = mapper;

        public async Task<GetManyVehiclesResponse> Handle(
            GetManyVehiclesQuery getManyVehiclesQuery, 
            CancellationToken cancellationToken)
        {
            GetManyVehiclesResponse getManyVehiclesResponse = new();

            var (vehiclesEntities, paginationMetadata) = 
                await _vehicleRepository.GetManyVehiclesAsync(
                    getManyVehiclesQuery.PageNumber, 
                    getManyVehiclesQuery.PageSize,
                    getManyVehiclesQuery.SearchQuery, 
                    getManyVehiclesQuery.VehicleName
                );

            getManyVehiclesResponse.PaginationMetadata = paginationMetadata;

            getManyVehiclesResponse.Vehicles = 
                _mapper.Map<ICollection<GetManyVehiclesDTO>>(vehiclesEntities);

            return getManyVehiclesResponse;
        }
    }
}