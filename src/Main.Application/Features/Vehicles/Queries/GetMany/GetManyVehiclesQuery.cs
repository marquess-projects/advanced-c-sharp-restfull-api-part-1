using MediatR;

namespace Main.Application.Features.Vehicles.Queries.GetMany
{
    public class GetManyVehiclesQuery 
        : IRequest<GetManyVehiclesResponse>
    {
        public int PageNumber { get; private set; }
        public int PageSize { get; private  set; }
        public string SearchQuery { get; private  set; }
        public string VehicleName { get; private  set; }

        public GetManyVehiclesQuery(
            int pageNumber,
            int pageSize,
            string searchQuery,
            string vehicleName)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchQuery = searchQuery;
            VehicleName = vehicleName;
        }
    }
}