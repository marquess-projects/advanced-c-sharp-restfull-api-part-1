using Main.Application.Features.Models.Queries.GetMany;

namespace Main.Application.Features.Vehicles.Queries.GetMany 
{
    public class GetManyVehiclesDTO 
    {
        public Guid Id { get; init; }
        public string Name { get; init; } = null!;
        public GetManyModelsDTO Model { get; init; } = null!;
    }
}