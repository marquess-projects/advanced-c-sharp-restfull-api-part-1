using Main.Application.Features.Common;

namespace Main.Application.Features.Vehicles.Queries.GetMany
{
    public class GetManyVehiclesResponse : BaseResponse
    {
        public ICollection<GetManyVehiclesDTO> Vehicles {get;set;} = null!;

        public PaginationMetadata PaginationMetadata = null!;
    }
}