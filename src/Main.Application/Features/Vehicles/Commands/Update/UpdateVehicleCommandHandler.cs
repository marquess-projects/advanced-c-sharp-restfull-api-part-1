using AutoMapper;
using Main.Application.Contracts;
using Main.Application.Features.Common;
using FluentValidation.Results;
using MediatR;

namespace Main.Application.Features.Vehicles.Commands.Update
{
    public class UpdateVehicleCommandHandler(
        IVehicleRepository vehicleRepository,
        IMapper mapper)
        : IRequestHandler<UpdateVehicleCommand, UpdateVehicleCommandResponse>
    {
        private readonly IVehicleRepository _vehicleRepository = vehicleRepository;

        private readonly IMapper _mapper = mapper;

        public async Task<UpdateVehicleCommandResponse> Handle(
            UpdateVehicleCommand updateVehicleCommand,
            CancellationToken cancellationToken)
        {
            var validator = new UpdateVehicleCommandValidator();

            var validationResult = await validator.ValidateAsync(
                updateVehicleCommand, cancellationToken
            );

            if (validationResult.IsValid == false)
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.ValidationProblem
                );
            }

            var vehicleEntity = await _vehicleRepository.GetOneVehicleAsync(
                updateVehicleCommand.Id
            );

            if (vehicleEntity is null)
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.NotFoundProblem,
                    "The vehicle with the provided ID does not exist."
                );
            }

            var modelId = updateVehicleCommand.ModelId;

            if (await _vehicleRepository.ModelExistsAsync(modelId) == false)
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.NotFoundProblem,
                    "The model with the provided ID does not exist."
                );
            }

            vehicleEntity.Update(
                updateVehicleCommand.Name,
                modelId
            );

            await _vehicleRepository.UnitOfWork.Commit();

            return new UpdateVehicleCommandResponse();
        }

        private static UpdateVehicleCommandResponse ErrorsResponse(
            ValidationResult validationResult,
            Error errorType,
            string? error = null)
        {
            var response = new UpdateVehicleCommandResponse();

            if (error != null) {
                validationResult.Errors.Add(
                    new ValidationFailure(
                        "Vehicle", error
                    )
                );
            }
            
            response.AddErrors(validationResult, errorType);

            return response;
        } 
    }
}