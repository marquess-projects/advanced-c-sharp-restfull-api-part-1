using Main.Domain.Entities.ModelEntity;
using MediatR;

namespace Main.Application.Features.Vehicles.Commands.Update
{
    public record UpdateVehicleCommand(Guid Id)
        : IRequest<UpdateVehicleCommandResponse>
    {
        public string Name { get; init; } = null!;
        public Guid ModelId { get; init; }
    }
}