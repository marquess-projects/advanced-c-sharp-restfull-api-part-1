using FluentValidation;

namespace Main.Application.Features.Vehicles.Commands.Update
{
    public class UpdateVehicleCommandValidator
        : AbstractValidator<UpdateVehicleCommand>
    {
        public UpdateVehicleCommandValidator()
        {
            RuleFor(e => e.Id)
                .NotEmpty()
                .WithMessage("{PropertyName} is rquired");
            
            RuleFor(e => e.ModelId)
                .NotEmpty()
                .WithMessage("{PropertyName} is required.");
        }
    }
}