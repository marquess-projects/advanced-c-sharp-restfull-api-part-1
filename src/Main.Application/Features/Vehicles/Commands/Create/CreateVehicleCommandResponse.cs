using Main.Application.Features.Common;

namespace Main.Application.Features.Vehicles.Commands.Create
{
    public class CreateVehicleCommandResponse : BaseResponse
    {
        public CreateVehicleDTO Vehicle { get; set; } = default!;
    }
}