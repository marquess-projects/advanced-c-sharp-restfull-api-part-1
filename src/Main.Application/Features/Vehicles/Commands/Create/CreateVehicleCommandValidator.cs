using FluentValidation;

namespace Main.Application.Features.Vehicles.Commands.Create
{
    public class CreateVehicleCommandValidator
        : AbstractValidator<CreateVehicleCommand>
    {
        public CreateVehicleCommandValidator()
        {
            RuleFor(e => e.Name)
                .NotEmpty()
                .WithMessage("{PropertyName} is required.");

            RuleFor(e => e.ModelId)
                .NotEmpty()
                .WithMessage("{PropertyName} is required.");
        }
    }
}