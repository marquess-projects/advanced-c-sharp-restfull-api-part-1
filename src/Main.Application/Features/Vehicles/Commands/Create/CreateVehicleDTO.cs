using Main.Application.Features.Models.Commands.Create;
namespace Main.Application.Features.Vehicles.Commands.Create
{
    public class CreateVehicleDTO
    {
        public Guid Id { get; init; }
        public string Name { get; init; } = null!;
        public CreateModelDTO Model { get; set; } = null!;
    }
}