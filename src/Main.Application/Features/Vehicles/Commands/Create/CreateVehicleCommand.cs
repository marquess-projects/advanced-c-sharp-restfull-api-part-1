using Main.Domain.Entities.ModelEntity;
using MediatR;

namespace Main.Application.Features.Vehicles.Commands.Create
{
    public record CreateVehicleCommand
        : IRequest<CreateVehicleCommandResponse>
    {
        public string Name { get; init; } = null!;
        public Guid ModelId { get; init; }
    }
}