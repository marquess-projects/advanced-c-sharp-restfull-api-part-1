using AutoMapper;
using FluentValidation.Results;
using Main.Application.Contracts;
using Main.Application.Features.Common;
using Main.Domain.Entities.VehicleEntity;
using MediatR;

namespace Main.Application.Features.Vehicles.Commands.Create
{
    public class CreateVehicleCommandHandler(
        IVehicleRepository vehicleRepository,
        IMapper mapper)
        : IRequestHandler<CreateVehicleCommand, CreateVehicleCommandResponse>
    {
        private readonly IVehicleRepository _vehicleRepository = vehicleRepository;

        private readonly IMapper _mapper = mapper;

        public async Task<CreateVehicleCommandResponse> Handle(
            CreateVehicleCommand createVehicleCommand,
            CancellationToken cancellationToken)
        {
            var validator = new CreateVehicleCommandValidator();

            var validationResult = await validator.ValidateAsync(
                createVehicleCommand, cancellationToken
            );

            if (validationResult.IsValid == false)
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.ValidationProblem
                );
            }

            var modelEntity = await _vehicleRepository
                .GetOneModelAsync(createVehicleCommand.ModelId);

            if (modelEntity is null)
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.NotFoundProblem,
                    "The brand with the provided ID does not exist."
                );
            }

            var newVehicle = new Vehicle (
                createVehicleCommand.Name,
                modelEntity
            );

            _vehicleRepository.AddOne(newVehicle);

            await _vehicleRepository.UnitOfWork.Commit();

            return new CreateVehicleCommandResponse()
            {
                Vehicle = _mapper.Map<CreateVehicleDTO>(newVehicle)
            };
        }

        private static CreateVehicleCommandResponse ErrorsResponse(
            ValidationResult validationResult,
            Error errorType,
            string? error = null)
        {
            var response = new CreateVehicleCommandResponse();

            if (error != null) {
                validationResult.Errors.Add(
                    new ValidationFailure(
                        "Vehicle", error
                    )
                );
            }
            
            response.AddErrors(validationResult, errorType);

            return response;
        }
    }
}