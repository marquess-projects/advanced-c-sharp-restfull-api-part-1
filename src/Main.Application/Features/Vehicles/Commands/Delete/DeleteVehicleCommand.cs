using MediatR;

namespace Main.Application.Features.Vehicles.Commands.Delete
{
    public record DeleteVehicleCommand(Guid Id) 
        : IRequest<DeleteVehicleCommandResponse>
    {
    }
}