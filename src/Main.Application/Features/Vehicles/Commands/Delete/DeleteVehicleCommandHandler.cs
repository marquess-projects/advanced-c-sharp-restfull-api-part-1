using AutoMapper;
using Main.Application.Contracts;
using Main.Application.Features.Common;
using FluentValidation.Results;
using MediatR;

namespace Main.Application.Features.Vehicles.Commands.Delete
{
    public class DeleteVehicleCommandHandler(
        IVehicleRepository vehicleRepository,
        IMapper mapper)
        : IRequestHandler<DeleteVehicleCommand, DeleteVehicleCommandResponse>
    {
        private readonly IVehicleRepository _vehicleRepository = vehicleRepository;

        private readonly IMapper _mapper = mapper;

        public async Task<DeleteVehicleCommandResponse> Handle(
            DeleteVehicleCommand deleteVehicleCommand,
            CancellationToken cancellationToken)
        {
            var validator = new DeleteVehicleCommandValidator();

            var validationResult = await validator.ValidateAsync(
                deleteVehicleCommand, cancellationToken
            );

            if (validationResult.IsValid == false)
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.NotFoundProblem
                );
            }

            var vehicleEntity = await _vehicleRepository.GetOneVehicleAsync(
                deleteVehicleCommand.Id
            );

            if (vehicleEntity is null)
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.NotFoundProblem,
                    "The vehicle with the provided ID does not exist."
                );
            }

            _vehicleRepository.RemoveOne(vehicleEntity);

            await _vehicleRepository.UnitOfWork.Commit();

            return new DeleteVehicleCommandResponse();
        }

        private static DeleteVehicleCommandResponse ErrorsResponse(
            ValidationResult validationResult,
            Error errorType,
            string? error = null)
        {
            var response = new DeleteVehicleCommandResponse();

            if (error != null) {
                validationResult.Errors.Add(
                    new ValidationFailure(
                        "Vehicle", error
                    )
                );
            }
            
            response.AddErrors(validationResult, errorType);

            return response;
        }
    }
}