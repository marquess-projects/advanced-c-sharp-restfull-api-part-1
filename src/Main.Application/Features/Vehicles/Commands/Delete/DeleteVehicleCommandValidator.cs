using FluentValidation;

namespace Main.Application.Features.Vehicles.Commands.Delete
{
    public class DeleteVehicleCommandValidator
        : AbstractValidator<DeleteVehicleCommand>
    {
        public DeleteVehicleCommandValidator()
        {
            RuleFor(e => e.Id)
                .NotEmpty()
                .WithMessage("{PropertyName} is rquired");
        }
    }
}