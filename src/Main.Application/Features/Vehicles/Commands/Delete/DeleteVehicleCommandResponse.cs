using Main.Application.Features.Common;

namespace Main.Application.Features.Vehicles.Commands.Delete
{
    public class DeleteVehicleCommandResponse : BaseResponse
    {
    }
}