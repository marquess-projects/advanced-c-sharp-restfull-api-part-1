using MediatR;

namespace Main.Application.Features.Users.Queries.GetMany
{
    public class GetManyUsersQuery 
        : IRequest<GetManyUsersQueryResponse>
    {
    }
}