using FluentValidation;

namespace Main.Application.Features.Users.Queries.GetMany 
{
    public class GetManyUsersQueryValidator
        : AbstractValidator<GetManyUsersQuery>
    {
    }
}