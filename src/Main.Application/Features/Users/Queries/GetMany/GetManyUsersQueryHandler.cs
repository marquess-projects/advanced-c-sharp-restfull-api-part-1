using System.ComponentModel.DataAnnotations;
using AutoMapper;
using FluentValidation.Results;
using Main.Application.Contracts;
using Main.Application.Features.Common;
using MediatR;

namespace Main.Application.Features.Users.Queries.GetMany
{
    public class GetManyUsersQueryHandler(
        IUserRepository userRepository,
        IMapper mapper)
        : IRequestHandler<GetManyUsersQuery, GetManyUsersQueryResponse>
    {
        private readonly IUserRepository _userRepository = userRepository;

        private readonly IMapper _mapper = mapper;

        public async Task<GetManyUsersQueryResponse> Handle(
            GetManyUsersQuery getManyUsersQuery, 
            CancellationToken cancellationToken)
        {
            var response = new GetManyUsersQueryResponse();

            var validator = new GetManyUsersQueryValidator();

            var validationResult = await validator.ValidateAsync(
                getManyUsersQuery, cancellationToken
            );

            var usersEntities = await _userRepository.GetAllAsync();

            response.Users = _mapper
                .Map<ICollection<GetManyUsersDTO>>(usersEntities);

            if (response.Users.Count == 0)
            {
                validationResult.Errors.Add(
                    new ValidationFailure(
                        "User", "There are no users."
                    )
                );
                
                response.AddErrors(validationResult, Error.NotFoundProblem);

                return response;
            }

            return response;
        }
    }
}