using Main.Application.Features.Common;

namespace Main.Application.Features.Users.Queries.GetMany
{
    public class GetManyUsersQueryResponse : BaseResponse
    {
        public ICollection<GetManyUsersDTO> Users {get;set;} = null!;
    }
}