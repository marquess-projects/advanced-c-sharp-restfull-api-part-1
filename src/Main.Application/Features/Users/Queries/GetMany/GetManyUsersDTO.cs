using Main.Domain.Entities.ModelEntity;
using Main.Domain.Entities.UserEntity;

namespace Main.Application.Features.Users.Queries.GetMany 
{
    public class GetManyUsersDTO 
    {
        public Guid Id { get; init; }
        
        public string Name { get; set; } = null!;
        
        public string Username { get; set; } = null!;

        public Clearance Clearance { get; set; }
    }
}