using AutoMapper;
using FluentValidation.Results;
using Main.Application.Contracts;
using Main.Application.Features.Common;
using MediatR;

namespace Main.Application.Features.Users.Queries.GetOne
{
    public class GetOneUserByUsernameQueryHandler(
        IUserRepository userRepository,
        IMapper mapper)
        : IRequestHandler<GetOneUserByUsernameQuery, GetOneUserByUsernameQueryResponse>
    {
        private readonly IUserRepository _userRepository = userRepository;

        private readonly IMapper _mapper = mapper;

        public async Task<GetOneUserByUsernameQueryResponse> Handle(
            GetOneUserByUsernameQuery getOneUserQuery, 
            CancellationToken cancellationToken)
        {
            var response = new GetOneUserByUsernameQueryResponse();

            var validator = new GetOneUserByUsernameQueryValidator();

            var validationResult = await validator
                .ValidateAsync(getOneUserQuery, cancellationToken);

            if (validationResult.IsValid == false)
            {
                response.AddErrors(validationResult, Error.ValidationProblem);

                return response;
            }

            var UserEntity = await _userRepository
                .GetOneByUsernameAsync(getOneUserQuery.Username);

            if (UserEntity is null)
            {
                validationResult.Errors.Add(
                    new ValidationFailure(
                        "User", "The user with the provided ID does not exist."
                    )
                );

                response.AddErrors(validationResult, Error.NotFoundProblem);

                return response;
            }

            response.User = _mapper.Map<GetOneUserByUsernameDTO>(UserEntity);

            return response;
        }
    }
}