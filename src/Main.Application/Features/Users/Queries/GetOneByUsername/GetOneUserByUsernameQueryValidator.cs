using FluentValidation;

namespace Main.Application.Features.Users.Queries.GetOne
{
    public class GetOneUserByUsernameQueryValidator 
        : AbstractValidator<GetOneUserByUsernameQuery>
    {
        public GetOneUserByUsernameQueryValidator()
        {
            RuleFor(e => e.Username)
                .NotEmpty()
                .WithMessage("{PropertyName} is required.");
        }
    }
}
