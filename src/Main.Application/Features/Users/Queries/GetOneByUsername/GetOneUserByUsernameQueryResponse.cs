using Main.Application.Features.Common;

namespace Main.Application.Features.Users.Queries.GetOne
{
    public class GetOneUserByUsernameQueryResponse : BaseResponse
    {
        public GetOneUserByUsernameDTO User {get;set;} = null!;
    }
}