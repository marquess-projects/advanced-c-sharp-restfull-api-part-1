using MediatR;

namespace Main.Application.Features.Users.Queries.GetOne
{
    public record GetOneUserByUsernameQuery(string Username)
        : IRequest<GetOneUserByUsernameQueryResponse>
    {
    }
}