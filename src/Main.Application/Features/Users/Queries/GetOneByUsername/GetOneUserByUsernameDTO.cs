using Main.Domain.Entities.UserEntity;

namespace Main.Application.Features.Users.Queries.GetOne 
{
    public class GetOneUserByUsernameDTO 
    {
        public Guid Id { get; init; }
        
        public string Name { get; set; } = null!;
        
        public string Username { get; set; } = null!;

        public string Password { get; set; } = null!;

        public Clearance Clearance { get; set; }
    }
}