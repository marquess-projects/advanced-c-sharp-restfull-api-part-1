using Main.Application.Features.Common;

namespace Main.Application.Features.Users.Commands.Create
{
    public class CreateUserCommandResponse : BaseResponse
    {
        public CreateUserDTO User { get; set; } = default!;
    }
}