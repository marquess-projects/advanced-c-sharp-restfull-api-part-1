using Main.Domain.Entities.ModelEntity;
using Main.Domain.Entities.UserEntity;
using Main.Domain.Entities.VehicleEntity;

namespace Main.Application.Features.Users.Commands.Create
{
    public class CreateUserDTO
    {
        public Guid Id { get; init; }

        public string Name { get; set; } = null!;
        
        public string Username { get; set; } = null!;
                
        public Clearance Clearance { get; set; }
    }
}