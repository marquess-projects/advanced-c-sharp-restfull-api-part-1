using System.Security.Cryptography;
using System.Text;
using Main.Domain.Entities.ModelEntity;
using Main.Domain.Entities.UserEntity;
using MediatR;

namespace Main.Application.Features.Users.Commands.Create
{
    public record CreateUserCommand
        : IRequest<CreateUserCommandResponse>
    {
        public string Name { get; set; } = null!;
        
        public string Username { get; set; } = null!;
        
        public string Password { get; set; } = null!;
        
        public Clearance Clearance { get; set; }
    }
}
