using AutoMapper;
using FluentValidation.Results;
using Main.Application.Contracts;
using Main.Application.Features.Common;
using Main.Core.Utils;
using Main.Domain.Entities.UserEntity;
using MediatR;

namespace Main.Application.Features.Users.Commands.Create
{
    public class CreateUserCommandHandler(
        IUserRepository userRepository,
        IMapper mapper,
        HashPassword hashPassword)
        : IRequestHandler<CreateUserCommand, CreateUserCommandResponse>
    {
        private readonly IUserRepository _userRepository = userRepository;

        private readonly IMapper _mapper = mapper;

        private readonly HashPassword _hashPassword = hashPassword;


        public async Task<CreateUserCommandResponse> Handle(
            CreateUserCommand createUserCommand,
            CancellationToken cancellationToken)
        {
            var validator = new CreateUserCommandValidator();

            var validationResult = await validator.ValidateAsync(
                createUserCommand, cancellationToken
            );

            if (validationResult.IsValid == false)
            {
                return ErrorsResponse(validationResult, Error.ValidationProblem);
            }

            if(!Enum.IsDefined(typeof(Clearance), createUserCommand.Clearance))
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.BadRequestProblem,
                    "Invalid clearance level."
                );
            }

            if (await _userRepository.UsernameExistsAsync(
                createUserCommand.Username)) 
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.BadRequestProblem,
                    "The selected username is being used."
                );
            }

            var newUser = new User (
                createUserCommand.Name,
                createUserCommand.Username,
                _hashPassword.From(createUserCommand.Password),
                createUserCommand.Clearance
            );

            _userRepository.AddOne(newUser);

            await _userRepository.SaveChanges();

            return new CreateUserCommandResponse
            {
                User = _mapper.Map<CreateUserDTO>(newUser)
            };
        }

        private static CreateUserCommandResponse ErrorsResponse(
            ValidationResult validationResult,
            Error errorType,
            string? error = null)
        {
            var response = new CreateUserCommandResponse();

            if (error != null) {
                validationResult.Errors.Add(
                    new ValidationFailure(
                        "User", error
                    )
                );
            }
            
            response.AddErrors(validationResult, errorType);

            return response;
        }
    }
}