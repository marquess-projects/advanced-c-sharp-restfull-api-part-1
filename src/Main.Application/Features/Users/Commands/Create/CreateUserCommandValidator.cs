using FluentValidation;

namespace Main.Application.Features.Users.Commands.Create
{
    public class CreateUserCommandValidator
        : AbstractValidator<CreateUserCommand>
    {
        public CreateUserCommandValidator()
        {
            RuleFor(e => e.Name)
                .NotEmpty()
                .WithMessage("{PropertyName} is required.");

            RuleFor(e => e.Username)
                .NotEmpty()
                .WithMessage("{PropertyName} is required.");

            RuleFor(e => e.Password)
                .NotEmpty()
                .WithMessage("{PropertyName} is required.");
        }
    }
}