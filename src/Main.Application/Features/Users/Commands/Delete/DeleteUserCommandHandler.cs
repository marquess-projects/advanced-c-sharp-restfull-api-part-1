using AutoMapper;
using FluentValidation.Results;
using Main.Application.Contracts;
using Main.Application.Features.Common;
using MediatR;

namespace Main.Application.Features.Users.Commands.Delete
{
    public class DeleteUserCommandHandler(
        IUserRepository userRepository,
        IMapper mapper)
        : IRequestHandler<DeleteUserCommand, DeleteUserCommandResponse>
    {
        private readonly IUserRepository _userRepository = userRepository;

        private readonly IMapper _mapper = mapper;

        public async Task<DeleteUserCommandResponse> Handle(
            DeleteUserCommand deleteUserCommand,
            CancellationToken cancellationToken)
        {
            var validator = new DeleteUserCommandValidator();

            var validationResult = await validator.ValidateAsync(
                deleteUserCommand, cancellationToken
            );

            if (validationResult.IsValid == false)
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.BadRequestProblem,
                    "The user with the provided ID does not exist."
                );
            }

            var userFromDatabase = await _userRepository
                .GetOneByIdAsync(deleteUserCommand.Id);
                
            if(userFromDatabase is null) 
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.BadRequestProblem,
                    "The user with the provided ID does not exist."
                );
            }

            _userRepository.DeleteOne(userFromDatabase);

            await _userRepository.SaveChanges();

            return new DeleteUserCommandResponse();
        }

        private static DeleteUserCommandResponse ErrorsResponse(
            ValidationResult validationResult,
            Error errorType,
            string? error = null)
        {
            var response = new DeleteUserCommandResponse();

            if (error != null) {
                validationResult.Errors.Add(
                    new ValidationFailure(
                        "User", error
                    )
                );
            }
            
            response.AddErrors(validationResult, errorType);

            return response;
        }
    }
}