using MediatR;

namespace Main.Application.Features.Users.Commands.Delete
{
    public record DeleteUserCommand(Guid Id) 
        : IRequest<DeleteUserCommandResponse>
    {
    }
}