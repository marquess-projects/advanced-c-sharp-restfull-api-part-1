using FluentValidation;

namespace Main.Application.Features.Users.Commands.Update
{
    public class UpdateUserCommandValidator
        : AbstractValidator<UpdateUserCommand>
    {
        public UpdateUserCommandValidator()
        {
            RuleFor(e => e.Name)
                .NotEmpty()
                .WithMessage("{PropertyName} is required.");

            RuleFor(e => e.Password)
                .NotEmpty()
                .WithMessage("{PropertyName} is required.");
        }
    }
}