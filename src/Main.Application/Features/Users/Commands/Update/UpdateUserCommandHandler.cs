using Main.Application.Contracts;
using Main.Application.Features.Common;
using FluentValidation.Results;
using MediatR;
using Main.Domain.Entities.UserEntity;
using Main.Core.Utils;

namespace Main.Application.Features.Users.Commands.Update
{
    public class UpdateUserCommandHandler(
        IUserRepository userRepository,
        HashPassword hashPassword)
        : IRequestHandler<UpdateUserCommand, UpdateUserCommandResponse>
    {
        private readonly IUserRepository _userRepository = userRepository;

        private readonly HashPassword _hashPassword = hashPassword;

        public async Task<UpdateUserCommandResponse> Handle(
            UpdateUserCommand updateUserCommand,
            CancellationToken cancellationToken)
        {
            var response = new UpdateUserCommandResponse();

            var validator = new UpdateUserCommandValidator();

            var validationResult = await validator.ValidateAsync(
                updateUserCommand, cancellationToken
            );

            if (validationResult.IsValid == false)
            {
                return ErrorsResponse(validationResult, Error.ValidationProblem);
            }

            if(!Enum.IsDefined(typeof(Clearance), updateUserCommand.Clearance))
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.BadRequestProblem,
                    "Invalid clearance level."
                );
            }

            var userEntity = await _userRepository
                .GetOneByIdAsync(updateUserCommand.Id);

            if (userEntity is null)
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.BadRequestProblem,
                    "The user with the provided ID does not exist."
                );
            }

            userEntity.Update(
                updateUserCommand.Name,
                _hashPassword.From(updateUserCommand.Password),
                updateUserCommand.Clearance
            );

            await _userRepository.SaveChanges();

            return response;
        }

        private static UpdateUserCommandResponse ErrorsResponse(
            ValidationResult validationResult,
            Error errorType,
            string? error = null)
        {
            var response = new UpdateUserCommandResponse();

            if (error != null) {
                validationResult.Errors.Add(
                    new ValidationFailure(
                        "User", error
                    )
                );
            }
            
            response.AddErrors(validationResult, errorType);

            return response;
        }
    }
}