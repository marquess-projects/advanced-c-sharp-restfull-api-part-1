using Main.Domain.Entities.ModelEntity;
using Main.Domain.Entities.UserEntity;
using MediatR;

namespace Main.Application.Features.Users.Commands.Update
{
    public record UpdateUserCommand(Guid Id)
        : IRequest<UpdateUserCommandResponse>
    {
        public string Name { get; set; } = null!;
        
        public string Password { get; set; } = null!;
        
        public Clearance Clearance { get; set; }
    }
}


