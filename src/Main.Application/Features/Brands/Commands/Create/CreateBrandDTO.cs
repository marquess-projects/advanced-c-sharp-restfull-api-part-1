namespace Main.Application.Features.Brands.Commands.Create
{
    public class CreateBrandDTO
    {
        public Guid Id { get; init; }
        public string Name { get; init; } = null!;
    }
}