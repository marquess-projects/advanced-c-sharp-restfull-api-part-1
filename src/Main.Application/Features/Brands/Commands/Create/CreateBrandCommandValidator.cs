using FluentValidation;

namespace Main.Application.Features.Brands.Commands.Create
{
    public class CreateBrandCommandValidator
        : AbstractValidator<CreateBrandCommand>
    {
        public CreateBrandCommandValidator()
        {
            RuleFor(e => e.Name)
                .NotEmpty()
                .WithMessage("{PropertyName} is required.");
        }
    }
}