using Main.Domain.Entities.ModelEntity;
using MediatR;

namespace Main.Application.Features.Brands.Commands.Create
{
    public record CreateBrandCommand
        : IRequest<CreateBrandCommandResponse>
    {
        public string Name { get; init; } = null!;
    }
}