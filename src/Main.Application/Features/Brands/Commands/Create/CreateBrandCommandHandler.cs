using AutoMapper;
using FluentValidation.Results;
using Main.Application.Contracts;
using Main.Application.Features.Common;
using Main.Domain.Entities.BrandEntity;
using MediatR;

namespace Main.Application.Features.Brands.Commands.Create
{
    public class CreateBrandCommandHandler(
        IVehicleRepository vehicleRepository,
        IMapper mapper)
        : IRequestHandler<CreateBrandCommand, CreateBrandCommandResponse>
    {
        private readonly IVehicleRepository _vehicleRepository = vehicleRepository;

        private readonly IMapper _mapper = mapper;

        public async Task<CreateBrandCommandResponse> Handle(
            CreateBrandCommand createBrandCommand,
            CancellationToken cancellationToken)
        {
            var validator = new CreateBrandCommandValidator();

            var validationResult = await validator.ValidateAsync(
                createBrandCommand, cancellationToken
            );

            if (validationResult.IsValid == false)
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.ValidationProblem
                );
            }

            var newBrand = new Brand (
                createBrandCommand.Name
            );

            _vehicleRepository.AddOne(newBrand);

            await _vehicleRepository.UnitOfWork.Commit();

            return new CreateBrandCommandResponse()
            {
                Brand = _mapper.Map<CreateBrandDTO>(newBrand)
            };
        }

        private static CreateBrandCommandResponse ErrorsResponse(
            ValidationResult validationResult,
            Error errorType,
            string? error = null)
        {
            var response = new CreateBrandCommandResponse();

            if (error != null) {
                validationResult.Errors.Add(
                    new ValidationFailure(
                        "Brand", error
                    )
                );
            }
            
            response.AddErrors(validationResult, errorType);

            return response;
        }
    }
}