using Main.Application.Features.Common;

namespace Main.Application.Features.Brands.Commands.Create
{
    public class CreateBrandCommandResponse : BaseResponse
    {
        public CreateBrandDTO Brand { get; set; } = default!;
    }
}