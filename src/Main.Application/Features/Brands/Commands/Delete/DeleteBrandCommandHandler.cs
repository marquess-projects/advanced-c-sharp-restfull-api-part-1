using AutoMapper;
using FluentValidation.Results;
using Main.Application.Contracts;
using Main.Application.Features.Common;
using MediatR;

namespace Main.Application.Features.Brands.Commands.Delete
{
    public class DeleteBrandCommandHandler(
        IVehicleRepository vehicleRepository,
        IMapper mapper)
        : IRequestHandler<DeleteBrandCommand, DeleteBrandCommandResponse>
    {
        private readonly IVehicleRepository _vehicleRepository = vehicleRepository;

        private readonly IMapper _mapper = mapper;

        public async Task<DeleteBrandCommandResponse> Handle(
            DeleteBrandCommand deleteBrandCommand,
            CancellationToken cancellationToken)
        {
            var validator = new DeleteBrandCommandValidator();

            var validationResult = await validator.ValidateAsync(
                deleteBrandCommand, cancellationToken
            );

            if (validationResult.IsValid == false)
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.ValidationProblem
                );
            }

            var brandEntity = await _vehicleRepository.GetOneBrandAsync(
                deleteBrandCommand.Id
            );

            if (brandEntity is null)
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.NotFoundProblem,
                    "The brand with the provided ID does not exist."
                );
            }

            if (await _vehicleRepository.HasModelWithBrand(deleteBrandCommand.Id))
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.ValidationProblem,
                    "Exclusion is not possible because there are models" + 
                    " with the brand provided."
                );
            }

            _vehicleRepository.RemoveOne(brandEntity);

            await _vehicleRepository.UnitOfWork.Commit();

            return new DeleteBrandCommandResponse();
        }

        private static DeleteBrandCommandResponse ErrorsResponse(
            ValidationResult validationResult,
            Error errorType,
            string? error = null)
        {
            var response = new DeleteBrandCommandResponse();

            if (error != null) {
                validationResult.Errors.Add(
                    new ValidationFailure(
                        "Brand", error
                    )
                );
            }
            
            response.AddErrors(validationResult, errorType);

            return response;
        }
    }
}
