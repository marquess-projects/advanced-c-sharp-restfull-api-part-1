using MediatR;

namespace Main.Application.Features.Brands.Commands.Delete
{
    public record DeleteBrandCommand(Guid Id) 
        : IRequest<DeleteBrandCommandResponse>
    {
    }
}