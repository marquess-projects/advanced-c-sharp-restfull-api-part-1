using FluentValidation;

namespace Main.Application.Features.Brands.Commands.Delete
{
    public class DeleteBrandCommandValidator
        : AbstractValidator<DeleteBrandCommand>
    {
        public DeleteBrandCommandValidator()
        {
        }
    }
}