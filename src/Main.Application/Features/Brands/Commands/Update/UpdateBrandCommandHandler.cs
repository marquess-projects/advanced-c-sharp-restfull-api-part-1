using AutoMapper;
using Main.Application.Contracts;
using Main.Application.Features.Common;
using FluentValidation.Results;
using MediatR;

namespace Main.Application.Features.Brands.Commands.Update
{
    public class UpdateBrandCommandHandler(
        IVehicleRepository vehicleRepository,
        IMapper mapper)
        : IRequestHandler<UpdateBrandCommand, UpdateBrandCommandResponse>
    {
        private readonly IVehicleRepository _vehicleRepository = vehicleRepository;

        private readonly IMapper _mapper = mapper;

        public async Task<UpdateBrandCommandResponse> Handle(
            UpdateBrandCommand updateBrandCommand,
            CancellationToken cancellationToken)
        {
            var validator = new UpdateBrandCommandValidator();

            var validationResult = await validator.ValidateAsync(
                updateBrandCommand, cancellationToken
            );

            if (validationResult.IsValid == false)
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.ValidationProblem
                );
            }

            var brandEntity = await _vehicleRepository.GetOneBrandAsync(
                updateBrandCommand.Id
            );

            if (brandEntity is null)
            {
                return ErrorsResponse(
                    validationResult, 
                    Error.NotFoundProblem,
                    "The brand with the provided ID does not exist."
                );
            }

            brandEntity.Update(
                updateBrandCommand.Name
            );

            await _vehicleRepository.UnitOfWork.Commit();

            return new UpdateBrandCommandResponse();
        }

        private static UpdateBrandCommandResponse ErrorsResponse(
            ValidationResult validationResult,
            Error errorType,
            string? error = null)
        {
            var response = new UpdateBrandCommandResponse();

            if (error != null) {
                validationResult.Errors.Add(
                    new ValidationFailure(
                        "Brand", error
                    )
                );
            }
            
            response.AddErrors(validationResult, errorType);

            return response;
        }
    }
}