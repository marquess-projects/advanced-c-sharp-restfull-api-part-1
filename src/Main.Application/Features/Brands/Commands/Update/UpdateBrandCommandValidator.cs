using FluentValidation;

namespace Main.Application.Features.Brands.Commands.Update
{
    public class UpdateBrandCommandValidator
        : AbstractValidator<UpdateBrandCommand>
    {
        public UpdateBrandCommandValidator()
        {
            RuleFor(e => e.Id)
                .NotEmpty()
                .WithMessage("{PropertyName} is rquired");
            
            RuleFor(e => e.Name)
                .NotEmpty()
                .WithMessage("{PropertyName} is required.");
        }
    }
}