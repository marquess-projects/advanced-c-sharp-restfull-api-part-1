using MediatR;

namespace Main.Application.Features.Brands.Commands.Update
{
    public record UpdateBrandCommand(Guid Id)
        : IRequest<UpdateBrandCommandResponse>
    {
        public string Name { get; init; } = null!;
    }
}