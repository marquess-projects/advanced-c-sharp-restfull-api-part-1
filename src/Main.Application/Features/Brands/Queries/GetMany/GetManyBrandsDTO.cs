namespace Main.Application.Features.Brands.Queries.GetMany 
{
    public class GetManyBrandsDTO 
    {
        public Guid Id { get; init; }
        public string Name { get; init; } = null!;
    }
}