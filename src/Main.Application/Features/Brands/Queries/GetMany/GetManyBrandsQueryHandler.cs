using AutoMapper;
using Main.Application.Contracts;
using MediatR;

namespace Main.Application.Features.Brands.Queries.GetMany
{
    public class GetManyBrandsQueryHandler(
        IVehicleRepository vehicleRepository,
        IMapper mapper)
        : IRequestHandler<GetManyBrandsQuery, GetManyBrandsResponse>
    {
        private readonly IVehicleRepository _vehicleRepository = vehicleRepository;

        private readonly IMapper _mapper = mapper;

        public async Task<GetManyBrandsResponse> Handle(
            GetManyBrandsQuery getManyBrandsQuery, 
            CancellationToken cancellationToken)
        {
            GetManyBrandsResponse getManyBrandsResponse = new();

            var (BrandsEntities, paginationMetadata) = 
                await _vehicleRepository.GetManyBrandsAsync(
                    getManyBrandsQuery.PageNumber, 
                    getManyBrandsQuery.PageSize,
                    getManyBrandsQuery.SearchQuery, 
                    getManyBrandsQuery.BrandName
                );

            getManyBrandsResponse.PaginationMetadata = paginationMetadata;

            getManyBrandsResponse.Brands = 
                _mapper.Map<ICollection<GetManyBrandsDTO>>(BrandsEntities);

            return getManyBrandsResponse;
        }
    }
}