using Main.Application.Features.Common;

namespace Main.Application.Features.Brands.Queries.GetMany
{
    public class GetManyBrandsResponse : BaseResponse
    {
        public ICollection<GetManyBrandsDTO> Brands {get;set;} = null!;

        public PaginationMetadata PaginationMetadata = null!;
    }
}