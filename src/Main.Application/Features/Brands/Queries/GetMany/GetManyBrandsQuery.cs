using MediatR;

namespace Main.Application.Features.Brands.Queries.GetMany
{
    public class GetManyBrandsQuery 
        : IRequest<GetManyBrandsResponse>
    {
        public int PageNumber { get; private set; }
        public int PageSize { get; private  set; }
        public string SearchQuery { get; private  set; }
        public string BrandName { get; private  set; }

        public GetManyBrandsQuery(
            int pageNumber,
            int pageSize,
            string searchQuery,
            string brandName)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchQuery = searchQuery;
            BrandName = brandName;
        }
    }
}