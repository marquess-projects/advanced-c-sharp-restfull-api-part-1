using Main.Domain.Entities.ModelEntity;

namespace Main.Application.Features.Brands.Queries.GetOne
{
    public class GetOneBrandDTO
    {
        public Guid Id { get; init; }
        public string Name { get; init; } = null!;
    }
}