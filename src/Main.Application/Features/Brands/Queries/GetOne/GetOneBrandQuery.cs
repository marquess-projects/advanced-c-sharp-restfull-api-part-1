using MediatR;

namespace Main.Application.Features.Brands.Queries.GetOne;

public record GetOneBrandQuery(Guid Id) 
    : IRequest<GetOneBrandQueryResponse> { }