using FluentValidation;

namespace Main.Application.Features.Brands.Queries.GetOne;

public class GetOneBrandQueryValidator 
    : AbstractValidator<GetOneBrandQuery>
{
    public GetOneBrandQueryValidator()
    {
        RuleFor(e => e.Id)
            .NotEmpty()
            .WithMessage("{PropertyName} is required.");
    }
}