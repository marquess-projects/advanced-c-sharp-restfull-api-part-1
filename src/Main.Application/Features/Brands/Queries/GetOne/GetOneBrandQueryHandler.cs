using AutoMapper;
using FluentValidation.Results;
using Main.Application.Contracts;
using Main.Application.Features.Common;
using MediatR;

namespace Main.Application.Features.Brands.Queries.GetOne;

public class GetOneBrandQueryHandler(
    IVehicleRepository vehicleRepository,
    IMapper mapper)
    : IRequestHandler<GetOneBrandQuery, GetOneBrandQueryResponse>
{
    private readonly IVehicleRepository _vehicleRepository = vehicleRepository;

    private readonly IMapper _mapper = mapper;

    public async Task<GetOneBrandQueryResponse> Handle(
        GetOneBrandQuery getOneBrandQuery, 
        CancellationToken cancellationToken)
    {
        var response = new GetOneBrandQueryResponse();

        var validator = new GetOneBrandQueryValidator();

        var validationResult = await validator
            .ValidateAsync(getOneBrandQuery, cancellationToken);

        if (validationResult.IsValid == false)
        {
            response.AddErrors(validationResult, Error.ValidationProblem);

            return response;
        }

        var brandEntity = await _vehicleRepository
            .GetOneBrandAsync(getOneBrandQuery.Id);

        if (brandEntity is null)
        {
            validationResult.Errors.Add(
                new ValidationFailure(
                    "Brand", "The brand with the provided ID does not exist."
                )
            );

            response.AddErrors(validationResult, Error.NotFoundProblem);

            return response;
        }

        response.Brand = _mapper.Map<GetOneBrandDTO>(brandEntity);

        return response;
    }
}