using Main.Application.Features.Common;

namespace Main.Application.Features.Brands.Queries.GetOne;

public class GetOneBrandQueryResponse : BaseResponse
{
    public GetOneBrandDTO Brand { get; set; } = default!;
}