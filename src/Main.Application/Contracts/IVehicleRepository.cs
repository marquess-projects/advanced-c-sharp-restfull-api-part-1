using Main.Application.Features.Common;
using Main.Core.Data;
using Main.Domain.Entities.BrandEntity;
using Main.Domain.Entities.ModelEntity;
using Main.Domain.Entities.VehicleEntity;

namespace Main.Application.Contracts
{
    public interface IVehicleRepository : IRepository<Vehicle>
    {
        // ADD

        void AddOne(Vehicle vehicle);

        void AddOne(Model model);

        void AddOne(Brand brand);

        // DELETE

        void RemoveOne(Vehicle vehicle);

        void RemoveOne(Model model);

        void RemoveOne(Brand brand);

        // Exists

        Task<bool> ModelExistsAsync(Guid modelId);

        Task<bool> BrandExistsAsync(Guid brandId);

        // Has

        Task<bool> HasModelWithBrand(Guid brandId);

        Task<bool> HasVehicleWithModel(Guid modelId);

        // GET
        
        Task<Vehicle?> GetOneVehicleAsync(Guid vehicleId);

        Task<(ICollection<Vehicle>, PaginationMetadata)> GetManyVehiclesAsync(
            int pageNumber, 
            int pageSize,
            string searchQuery = "",
            string vehicleName = "" 
        );

        Task<Brand?> GetOneBrandAsync(Guid brandId);

        Task<(ICollection<Brand>, PaginationMetadata)> GetManyBrandsAsync(
            int pageNumber, 
            int pageSize,
            string searchQuery = "",
            string brandName = "" 
        );

        Task<Model?> GetOneModelAsync(Guid modelId);

        Task<(ICollection<Model>, PaginationMetadata)> GetManyModelsAsync(
            int pageNumber, 
            int pageSize,
            string searchQuery = "",
            string modelName = "" 
        );
    }
}