using Main.Domain.Entities.UserEntity;

namespace Main.Application.Contracts
{
    public interface IUserRepository
    {
        Task<bool> SaveChanges();

        void AddOne(User user);

        void DeleteOne(User user);

        Task<ICollection<User>> GetAllAsync();

        Task<User?> GetOneByIdAsync(Guid userId);
        
        Task<User?> GetOneByUsernameAsync(string username);

        Task<bool> UsernameExistsAsync(string username);
    }
}