using Main.Domain.Entities.UserEntity;

namespace Main.Application.Models
{
    public class UserDto
    {
        public Guid UserId { get; set; }

        public string Name { get; set; } = string.Empty;

        public string Username { get; set; } = string.Empty;
        
        public Clearance Clearance { get; set; }
    }
}


