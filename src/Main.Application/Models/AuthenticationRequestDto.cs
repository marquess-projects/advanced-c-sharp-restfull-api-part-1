using System.ComponentModel.DataAnnotations;

namespace Main.Application.Models
{
    public class AuthenticationRequestDto
    {
        [Required(ErrorMessage = "Name field is required")]
        public string Username { get; set; } = null!;

        [Required(ErrorMessage = "Password field is required")]
        public string Password { get; set; } = null!;
    }
}
