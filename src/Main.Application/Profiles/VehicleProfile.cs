using AutoMapper;
using Main.Application.Features.Brands.Commands.Create;
using Main.Application.Features.Brands.Queries.GetMany;
using Main.Application.Features.Brands.Queries.GetOne;
using Main.Application.Features.Models.Commands.Create;
using Main.Application.Features.Models.Queries.GetMany;
using Main.Application.Features.Models.Queries.GetOne;
using Main.Application.Features.Vehicles.Commands.Create;
using Main.Application.Features.Vehicles.Queries.GetMany;
using Main.Application.Features.Vehicles.Queries.GetOne;
using Main.Domain.Entities.BrandEntity;
using Main.Domain.Entities.ModelEntity;
using Main.Domain.Entities.VehicleEntity;

namespace Main.Application.Profiles
{
    public class VehicleProfile : Profile
    {
        public VehicleProfile()
        {
            // Vehicles
            CreateMap<Vehicle, CreateVehicleDTO>();
            CreateMap<Vehicle, GetOneVehicleDTO>();
            CreateMap<Vehicle, GetManyVehiclesDTO>();

            // Models
            CreateMap<Model, CreateModelDTO>();
            CreateMap<Model, GetOneModelDTO>();
            CreateMap<Model, GetManyModelsDTO>();

            // Brands
            CreateMap<Brand, CreateBrandDTO>();
            CreateMap<Brand, GetOneBrandDTO>();
            CreateMap<Brand, GetManyBrandsDTO>();
        }
    }
}