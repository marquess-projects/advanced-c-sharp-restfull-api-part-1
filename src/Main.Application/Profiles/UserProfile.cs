using AutoMapper;
using Main.Application.Features.Users.Commands.Create;
using Main.Application.Features.Users.Queries.GetMany;
using Main.Application.Features.Users.Queries.GetOne;
using Main.Application.Models;
using Main.Domain.Entities.UserEntity;

namespace Main.Application.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, CreateUserDTO>();

            CreateMap<User, GetOneUserByUsernameDTO>();
            
            CreateMap<User, GetManyUsersDTO>();

            CreateMap<GetOneUserByUsernameDTO, UserDto>();
        }
    }
}