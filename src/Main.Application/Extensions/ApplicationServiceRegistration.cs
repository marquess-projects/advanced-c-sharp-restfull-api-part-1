using System.Reflection;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using Main.Application.Features.Vehicles.Commands.Create;
using Main.Application.Features.Vehicles.Commands.Update;
using Main.Application.Features.Vehicles.Commands.Delete;
using Main.Application.Features.Vehicles.Queries.GetOne;
using Main.Application.Features.Models.Commands.Create;
using Main.Application.Features.Models.Commands.Update;
using Main.Application.Features.Models.Commands.Delete;
using Main.Application.Features.Models.Queries.GetOne;
using Main.Application.Features.Brands.Commands.Create;
using Main.Application.Features.Brands.Commands.Update;
using Main.Application.Features.Brands.Queries.GetOne;
using Main.Application.Features.Users.Commands.Create;
using Main.Application.Features.Users.Commands.Update;
using Main.Application.Features.Users.Commands.Delete;
using Main.Application.Features.Users.Queries.GetOne;
using Main.Core.Utils;
using Main.Application.Features.Brands.Commands.Delete;

namespace Main.Application.Extensions
{
    public static class ApplicationServiceRegistration
    {
        public static IServiceCollection AddApplicationServices(
            this IServiceCollection services)
        {
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddMediatR(cfg => 
                cfg.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly())
            );

            services.AddFluentValidationServices();

            services.AddSingleton<HashPassword>();

            return services;
        }

        private static void AddFluentValidationServices(
            this IServiceCollection services)
        {
            // Vehicle feature

            services.AddScoped
                <IValidator<CreateVehicleCommand>, CreateVehicleCommandValidator>();

            services.AddScoped
                <IValidator<UpdateVehicleCommand>, UpdateVehicleCommandValidator>();
            
            services.AddScoped
                <IValidator<DeleteVehicleCommand>, DeleteVehicleCommandValidator>();
            
            services.AddScoped
                <IValidator<GetOneVehicleQuery>, GetOneVehicleQueryValidator>();

            // Model feature

            services.AddScoped
                <IValidator<CreateModelCommand>, CreateModelCommandValidator>();

            services.AddScoped
                <IValidator<UpdateModelCommand>, UpdateModelCommandValidator>();
            
            services.AddScoped
                <IValidator<DeleteModelCommand>, DeleteModelCommandValidator>();
            
            services.AddScoped
                <IValidator<GetOneModelQuery>, GetOneModelQueryValidator>();

            // Brand feature

            services.AddScoped
                <IValidator<CreateBrandCommand>, CreateBrandCommandValidator>();

            services.AddScoped
                <IValidator<UpdateBrandCommand>, UpdateBrandCommandValidator>();

            services.AddScoped
                <IValidator<DeleteBrandCommand>, DeleteBrandCommandValidator>();
            
            services.AddScoped
                <IValidator<GetOneBrandQuery>, GetOneBrandQueryValidator>();

            // User

            services.AddScoped
                <IValidator<CreateUserCommand>, CreateUserCommandValidator>();

            services.AddScoped
                <IValidator<UpdateUserCommand>, UpdateUserCommandValidator>();
            
            services.AddScoped
                <IValidator<DeleteUserCommand>, DeleteUserCommandValidator>();
            
            services.AddScoped
                <IValidator<GetOneUserByUsernameQuery>, GetOneUserByUsernameQueryValidator>();
        }
    }
}