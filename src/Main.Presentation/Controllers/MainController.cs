using Main.Application.Features.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Main.Presentation.Controllers
{
    [ApiController]
    [Authorize]
    public abstract class MainController : ControllerBase
    {
        protected const int maxPageSize = 5;

        protected ActionResult HandleRequestError(BaseResponse response)
        {
            ConfigureModelState(response.Errors);

            return response.ErrorType switch
            {
                Error.ValidationProblem => UnprocessableEntity(ModelState),
                Error.NotFoundProblem => NotFound(ModelState),
                Error.BadRequestProblem => BadRequest(ModelState),
                _ => StatusCode(StatusCodes.Status500InternalServerError)
            };
        }

        private void ConfigureModelState(IDictionary<string, string[]> errors)
        {
            foreach (var error in errors)
            {
                string key = error.Key;
                
                string[] values = error.Value;

                foreach (var value in values)
                {
                    ModelState.AddModelError(key, value);
                }
            }
        }

        public override UnprocessableEntityObjectResult UnprocessableEntity(
            [ActionResultObjectValue] ModelStateDictionary modelState)
        {
            var problemDetailsFactory = HttpContext.RequestServices
                .GetRequiredService<ProblemDetailsFactory>();

            var validationProblemDetails = problemDetailsFactory
                .CreateValidationProblemDetails(
                    HttpContext,
                    ModelState);

            validationProblemDetails.Detail =
                "See the errors field for details.";
            validationProblemDetails.Instance =
                HttpContext.Request.Path;

            validationProblemDetails.Type =
                "https://allog.com.br/modelvalidationproblem";

            validationProblemDetails.Status =
                StatusCodes.Status422UnprocessableEntity;

            validationProblemDetails.Title =
                "One or more validation errors occurred.";

            return new UnprocessableEntityObjectResult(
                validationProblemDetails)
            {
                ContentTypes = { "application/problem+json" }
            };
        }


        public override NotFoundObjectResult NotFound(
            [ActionResultObjectValue] object? value)
        {
            var problemDetailsFactory = HttpContext.RequestServices
                .GetRequiredService<ProblemDetailsFactory>();

            var validationProblemDetails = problemDetailsFactory
                .CreateValidationProblemDetails(
                    HttpContext,
                    ModelState!);

            validationProblemDetails.Detail =
                "See the errors field for details.";

            validationProblemDetails.Instance =
                HttpContext.Request.Path;

            validationProblemDetails.Type =
                "https://allog.com.br/notfoundproblem";

            validationProblemDetails.Status =
                StatusCodes.Status404NotFound;

            validationProblemDetails.Title =
                "One or more records were not found";

            return new NotFoundObjectResult(
                validationProblemDetails)
            {
                ContentTypes = { "application/problem+json" }
            };

        }

        public override BadRequestObjectResult BadRequest(
            ModelStateDictionary modelStateDictionary)
        {
            var problemDetailsFactory = HttpContext.RequestServices
                .GetRequiredService<ProblemDetailsFactory>();

            var validationProblemDetails = problemDetailsFactory
                .CreateValidationProblemDetails(
                    HttpContext,
                    ModelState!);

            validationProblemDetails.Detail =
                "See the errors field for details.";

            validationProblemDetails.Instance =
                HttpContext.Request.Path;

            validationProblemDetails.Type =
                "https://allog.com.br/badrequestproblem";

            validationProblemDetails.Status =
                StatusCodes.Status400BadRequest;

            validationProblemDetails.Title =
                "One or more issues were found within the request body";

            return new BadRequestObjectResult(
                validationProblemDetails)
            {
                ContentTypes = { "application/problem+json" }
            };
        }
    }
}