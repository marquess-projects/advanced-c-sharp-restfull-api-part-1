using Main.Application.Features.Models.Commands.Create;
using Main.Application.Features.Models.Commands.Delete;
using Main.Application.Features.Models.Commands.Update;
using Main.Application.Features.Models.Queries.GetMany;
using Main.Application.Features.Models.Queries.GetOne;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace Main.Presentation.Controllers
{
    [Route("api/models")]
    public class ModelController(IMediator mediator) : MainController
    {
        private readonly IMediator _mediator = mediator;

        [HttpPost]
        [Authorize(Policy = "AdminPolicy")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        public async Task<ActionResult<CreateModelDTO>> CreateModel(
            CreateModelCommand createModelCommand)
        {
            var response = await _mediator.Send(createModelCommand);

            if (response.IsValid == false) 
            {
                return HandleRequestError(response); 
            }

            return CreatedAtRoute(
                "GetOneModel",
                new {modelId = response.Model.Id},
                response.Model
            );
        }

        [HttpPut("{modelId}")]
        [Authorize(Policy = "AdminPolicy")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> UpdateModel(
            Guid modelId, 
            UpdateModelCommand updateModelCommand)
        {
            if (modelId != updateModelCommand.Id)
            {
                return BadRequest();
            }

            var response = await _mediator.Send(updateModelCommand);

            if (response.IsValid == false) 
            {
                return HandleRequestError(response); 
            }

            return NoContent();
        }

        [HttpDelete("{modelId}")]
        [Authorize(Policy = "AdminPolicy")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteModel(Guid modelId)
        {
            var response = await _mediator.Send(
                new DeleteModelCommand(modelId)
            );

            if (response.IsValid == false)
            {
                return HandleRequestError(response); 
            }

            return NoContent();
        }

        [HttpGet("{modelId}", Name = "GetOneModel")]
        [Authorize(Policy = "StandardPolicy")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GetOneModelDTO>> GetOneModel(
            Guid modelId)
        {
            Console.WriteLine(modelId);

            var response = await _mediator.Send(
                new GetOneModelQuery(modelId) 
            );

            if (response.IsValid == false) 
            {
                return HandleRequestError(response); 
            }

            return Ok(response.Model);
        }  

        [HttpGet]
        [Authorize(Policy = "StandardPolicy")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ICollection<GetManyModelsDTO>>> GetManyModels(
            string modelName = "", 
            string searchQuery = "",
            int pageNumber = 1, 
            int pageSize = 5)
        {
            if (pageSize < 1 || pageSize > maxPageSize) {
                pageSize = maxPageSize;
            }

            var getManyModelsQueryResponse = await _mediator.Send(
                new GetManyModelsQuery(
                    pageNumber, pageSize, searchQuery, modelName
                )
            );

            Response.Headers.Append("X-Pagination", JsonSerializer.Serialize(
                getManyModelsQueryResponse.PaginationMetadata)
            );

            return Ok(getManyModelsQueryResponse.Models);
        }      
    }
}