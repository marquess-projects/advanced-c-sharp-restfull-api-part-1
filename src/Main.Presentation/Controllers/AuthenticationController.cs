using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using AutoMapper;
using Main.Application.Contracts;
using Main.Application.Features.Users.Commands.Create;
using Main.Application.Features.Users.Commands.Delete;
using Main.Application.Features.Users.Commands.Update;
using Main.Application.Features.Users.Queries.GetMany;
using Main.Application.Features.Users.Queries.GetOne;
using Main.Application.Models;
using Main.Domain.Entities.UserEntity;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace Main.Presentation.Controllers
{
    [Route("api/authentication")]
    public class AuthenticationController(
        IConfiguration configuration,
        IMediator mediator,
        IMapper mapper) : MainController
    {
        private readonly IMapper _mapper = mapper;

        private readonly IConfiguration _configuration = configuration;

        private readonly IMediator _mediator = mediator;

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<string>> Authenticate(
            AuthenticationRequestDto authenticationRequestDto)
        {
            var user = await ValidateUserCredentials(
                authenticationRequestDto.Username,
                authenticationRequestDto.Password
            );


            if (user == null) { return Unauthorized(); }
           
            var securityKey = new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(
                    _configuration["Authentication:SecretKey"]!
                )
            );

            // Chave de Assinatura
            var signingCredentials = new SigningCredentials(
                securityKey, 
                SecurityAlgorithms.HmacSha256
            );

            // Informações sobre quem o Token representa
            var claims = new List<Claim>();
            claims.Add(new Claim("sub", user.UserId.ToString()));
            claims.Add(new Claim("given_name", user.Name));
            claims.Add(new Claim("ClearanceLevel", user.Clearance.ToString()));

            var jwt = new JwtSecurityToken(
                _configuration["Authentication:Issuer"],
                _configuration["Authentication:Audience"],
                claims,
                // Início da validade do token. 
                DateTime.UtcNow,
                // Fim da validade do token. 
                DateTime.UtcNow.AddHours(1),
                signingCredentials
            );
            
            var jwtToReturn = new JwtSecurityTokenHandler().WriteToken(jwt);

            return Ok(jwtToReturn);
        }

        private async Task<UserDto?> ValidateUserCredentials(
            string username, 
            string password)
        {

            var response = await _mediator
                .Send(new GetOneUserByUsernameQuery(username));

            if (response.IsValid == false) 
            {
                return null;
            }
            
            if (VerifyPassword(password, response.User.Password))
            {
                return _mapper.Map<UserDto>(response.User);
            }

            return null;
        }

        private bool VerifyPassword(string enteredPassword, string storedPassword)
        {
            using var hmac = new HMACSHA256(
                Encoding.UTF8.GetBytes(
                    _configuration["Authentication:SecretKey"]!
                )
            );

            var computedHash = hmac.ComputeHash(
                Encoding.UTF8.GetBytes(enteredPassword)
            );

            return computedHash.SequenceEqual(
                Convert.FromBase64String(storedPassword)
            );
        }

        [HttpPost("users")]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        public async Task<ActionResult<CreateUserDTO>> CreateUser(
            CreateUserCommand createUserCommand)
        {
            var response = await _mediator.Send(createUserCommand);

            if (response.IsValid == false) 
            {
                return HandleRequestError(response); 
            }

            return Ok(response.User);
        }

        [HttpPut("users/{userId}")]
        [Authorize(Policy = "AdminPolicy")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> UpdateUser(
            Guid userId, 
            UpdateUserCommand updateUserCommand)
        {
            if (userId != updateUserCommand.Id)
            {
                return BadRequest("Mismatch UserId.");
            }

            var response = await _mediator.Send(updateUserCommand);

            if (response.IsValid == false) 
            {
                return HandleRequestError(response); 
            }

            return NoContent();
        }

        [HttpDelete("users/{userId}")]
        [Authorize(Policy = "AdminPolicy")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteUser(
            Guid userId, 
            DeleteUserCommand deleteUserCommand)
        {
            if (userId != deleteUserCommand.Id)
            {
                return BadRequest("Mismatch UserId.");
            }

            var response = await _mediator.Send(deleteUserCommand);

            if (response.IsValid == false) 
            {
                return HandleRequestError(response); 
            }

            return NoContent();
        }

        [HttpGet("users")]
        [Authorize(Policy = "AdminPolicy")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<GetManyUsersDTO>>> GetAllUsers()
        {
            var response = await _mediator.Send(new GetManyUsersQuery());

            if (response.IsValid == false) 
            {
                return HandleRequestError(response); 
            }

            return Ok(response.Users);
        }
    }   
}