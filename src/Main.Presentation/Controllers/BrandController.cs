using Main.Application.Features.Brands.Commands.Create;
using Main.Application.Features.Brands.Commands.Delete;
using Main.Application.Features.Brands.Commands.Update;
using Main.Application.Features.Brands.Queries.GetMany;
using Main.Application.Features.Brands.Queries.GetOne;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace Main.Presentation.Controllers
{
    [Route("api/brands")]
    public class BrandController(IMediator mediator) : MainController
    {
        private readonly IMediator _mediator = mediator;

        [HttpPost]
        [Authorize(Policy = "AdminPolicy")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        public async Task<ActionResult<CreateBrandDTO>> CreateBrand(
            CreateBrandCommand createBrandCommand)
        {
            var response = await _mediator.Send(createBrandCommand);

            if (response.IsValid == false) 
            {
                return HandleRequestError(response); 
            }

            return CreatedAtRoute(
                "GetOneBrand",
                new {brandId = response.Brand.Id},
                response.Brand
            );
        }

        [HttpPut("{brandId}")]
        [Authorize(Policy = "AdminPolicy")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> UpdateBrand(
            Guid brandId, 
            UpdateBrandCommand updateBrandCommand)
        {
            if (brandId != updateBrandCommand.Id)
            {
                return BadRequest();
            }

            var response = await _mediator.Send(updateBrandCommand);

            if (response.IsValid == false) 
            {
                return HandleRequestError(response); 
            }

            return NoContent();
        }

        [HttpDelete("{brandId}")]
        [Authorize(Policy = "AdminPolicy")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteBrand(Guid brandId)
        {
            var response = await _mediator.Send(
                new DeleteBrandCommand(brandId)
            );

            if (response.IsValid == false)
            {
                return HandleRequestError(response); 
            }

            return NoContent();
        }

        [HttpGet("{brandId}", Name = "GetOneBrand")]
        [Authorize(Policy = "StandardPolicy")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GetOneBrandDTO>> GetOneBrand(
            Guid brandId)
        {
            Console.WriteLine(brandId);

            var response = await _mediator.Send(
                new GetOneBrandQuery(brandId) 
            );

            if (response.IsValid == false) 
            {
                return HandleRequestError(response); 
            }

            return Ok(response.Brand);
        }  

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Authorize(Policy = "StandardPolicy")]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ICollection<GetManyBrandsDTO>>> GetManyBrands(
            int pageNumber = 1, 
            int pageSize = 5,
            string brandName = "", 
            string searchQuery = "")
        {
            if (pageSize < 1 || pageSize > maxPageSize) {
                pageSize = maxPageSize;
            }

            var getManyBrandsQueryResponse = await _mediator.Send(
                new GetManyBrandsQuery(
                    pageNumber, pageSize, searchQuery, brandName
                )
            );

            Response.Headers.Append("X-Pagination", JsonSerializer.Serialize(
                getManyBrandsQueryResponse.PaginationMetadata)
            );

            return Ok(getManyBrandsQueryResponse.Brands);
        }      
    }
}