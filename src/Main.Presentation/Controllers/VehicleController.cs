using Main.Application.Features.Vehicles.Commands.Create;
using Main.Application.Features.Vehicles.Commands.Delete;
using Main.Application.Features.Vehicles.Commands.Update;
using Main.Application.Features.Vehicles.Queries.GetMany;
using Main.Application.Features.Vehicles.Queries.GetOne;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace Main.Presentation.Controllers
{
    [Route("api/vehicles")]
    public class VehicleController : MainController
    {
        private readonly IMediator _mediator;

        public VehicleController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        [Authorize(Policy = "AdminPolicy")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        public async Task<ActionResult<CreateVehicleDTO>> CreateVehicle(
            CreateVehicleCommand createVehicleCommand)
        {
            var response = await _mediator.Send(createVehicleCommand);

            if (response.IsValid == false) 
            {
                return HandleRequestError(response); 
            }

            return CreatedAtRoute(
                "GetOneVehicle",
                new {vehicleId = response.Vehicle.Id},
                response.Vehicle
            );
        }

        [HttpPut("{vehicleId}")]
        [Authorize(Policy = "AdminPolicy")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> UpdateVehicle(
            Guid vehicleId, 
            UpdateVehicleCommand updateVehicleCommand)
        {
            if (vehicleId != updateVehicleCommand.Id)
            {
                return BadRequest();
            }

            var response = await _mediator.Send(updateVehicleCommand);

            if (response.IsValid == false) 
            {
                return HandleRequestError(response); 
            }

            return NoContent();
        }

        [HttpDelete("{vehicleId}")]
        [Authorize(Policy = "AdminPolicy")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteVehicle(Guid vehicleId)
        {
            var response = await _mediator.Send(
                new DeleteVehicleCommand(vehicleId)
            );

            if (response.IsValid == false)
            {
                return HandleRequestError(response); 
            }

            return NoContent();
        }

        [HttpGet("{vehicleId}", Name = "GetOneVehicle")]
        [Authorize(Policy = "StandardPolicy")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GetOneVehicleDTO>> GetOneVehicle(
            Guid vehicleId)
        {
            Console.WriteLine(vehicleId);

            var response = await _mediator.Send(
                new GetOneVehicleQuery(vehicleId) 
            );

            if (response.IsValid == false) 
            {
                return HandleRequestError(response); 
            }

            return Ok(response.Vehicle);
        }  

        [HttpGet]
        [Authorize(Policy = "StandardPolicy")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ICollection<GetManyVehiclesDTO>>> GetManyVehicles(
            string vehicleName = "", 
            string searchQuery = "",
            int pageNumber = 1, 
            int pageSize = 5)
        {
            if (pageSize < 1 || pageSize > maxPageSize) {
                pageSize = maxPageSize;
            }

            var getManyVehiclesQueryResponse = await _mediator.Send(
                new GetManyVehiclesQuery(
                    pageNumber, pageSize, searchQuery, vehicleName
                )
            );

            Response.Headers.Append("X-Pagination", JsonSerializer.Serialize(
                getManyVehiclesQueryResponse.PaginationMetadata)
            );

            return Ok(getManyVehiclesQueryResponse.Vehicles);
        }      
    }
}