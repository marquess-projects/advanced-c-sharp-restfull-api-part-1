// dotnet ef --startup-project ../Main.Presentation/ migrations add InitialMigration -o Migrations/User --context UserContext
// dotnet ef --startup-project ../Main.Presentation/ database update --context UserContext
// dotnet ef --startup-project ../Main.Presentation/ migrations add InitialMigration -o Migrations/Vehicle --context VehicleContext
// dotnet ef --startup-project ../Main.Presentation/ database update --context VehicleContext

using Main.Infrastructure.Persistence.Extensions;
using Main.Application.Extensions;
using Main.Infrastructure.Persistence.Contexts;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Microsoft.EntityFrameworkCore;
using Main.Presentation.Extensions;
using System.Text.Json.Serialization;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        builder.Services.AddPoliciesServices();

        builder.Services.AddApplicationServices();

        builder.Services.AddPersistenceServices(builder.Configuration);

        // Add services to the container.
        builder.Services.AddControllers() // Important
        .AddJsonOptions(opt => {
            opt.JsonSerializerOptions.Converters.Add(
                new JsonStringEnumConverter()
            );
        });

        builder.Services.AddEndpointsApiExplorer();
        
        builder.Services.AddSwaggerGen();

        builder.Services.AddAuthentication("Bearer").AddJwtBearer(options =>
        {
            options.TokenValidationParameters = new()
            {
                // Obriga a validação do emissor
                ValidateIssuer = true,
                // Obriga a validação da audiência
                ValidateAudience = true,
                // Obriga a validação da chave de assinatura`
                ValidateIssuerSigningKey = true,
                // Apenas tokens  gerados por esta api serão considerados válidos.
                ValidIssuer = builder.Configuration["Authentication:Issuer"],
                // Apenas tokens desta audiência serão considerados válidos.
                ValidAudience = builder.Configuration["Authentication:Audience"],
                // Apenas tokens com essa assinatura serão considerados válidos.
                IssuerSigningKey = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(
                        builder.Configuration["Authentication:SecretKey"]!
                    )
                )
            };
        });

        var app = builder.Build();

        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment()) // impede persistência real
        {
            app.UseSwagger();

            app.UseSwaggerUI();

            ResetDatabase(app);
        }

        app.UseHttpsRedirection();

        app.UseAuthentication();

        app.UseAuthorization();

        app.MapControllers(); // Important

        app.Run();
    }

    private static void ResetDatabase(WebApplication app)
    {
        using var scope = app.Services.CreateScope();
        {
            var userContext = scope.ServiceProvider
                .GetRequiredService<UserContext>();

            userContext.Database.EnsureDeleted();
            userContext.Database.Migrate();
            userContext.Database.EnsureCreated();

            var vehicleContext = scope.ServiceProvider
                .GetRequiredService<VehicleContext>();

            vehicleContext.Database.Migrate();
            vehicleContext.Database.EnsureCreated();
        }
    }
}

