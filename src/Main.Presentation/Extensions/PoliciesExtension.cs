namespace Main.Presentation.Extensions
{
    public static class PoliciesExtension
    {
        public static IServiceCollection AddPoliciesServices(
            this IServiceCollection services)
        {
            services.AddAuthorizationBuilder()
                .AddPolicy("StandardPolicy", policy => {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim("ClearanceLevel", "Admin", "ViewOnly");
                })
                .AddPolicy("AdminPolicy", policy => {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim("ClearanceLevel", "Admin");
                });

            return services;

            //services.AddAuthorizationBuilder()
            //    .AddPolicy("LoginRequired", policy => {
            //        policy.RequireAuthenticatedUser();
            //    });
        }
    }
}