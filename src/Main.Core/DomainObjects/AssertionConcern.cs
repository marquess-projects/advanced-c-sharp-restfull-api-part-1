namespace Main.Core.DomainObjects
{
    public class AssertionConcern
    {
        public static void AssertArgumentGreaterThan(
            decimal value, 
            decimal minimum, string message)
        {
            if (value <= minimum)
            {
                throw new DomainException(message);
            }
        }

        public static void AssertArgumentNotEmpty(
            Guid id, 
            string message)
        {
            if (id.Equals(Guid.Empty))
            {
                throw new DomainException(message);
            }
        }

        public static void AssertArgumentNotEmpty(
            string argument, 
            string message)
        {
            if (argument.Equals(string.Empty))
            {
                throw new DomainException(message);
            }
        }
    }
}