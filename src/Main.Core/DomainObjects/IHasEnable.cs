namespace Main.Core.DomainObjects
{
    public interface IHasEnable
    {
        bool Enable { get; }
    }
}