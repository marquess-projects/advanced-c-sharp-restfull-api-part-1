using Microsoft.Extensions.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace Main.Core.Utils
{
    public class HashPassword(IConfiguration configuration)
    {
        private readonly IConfiguration _configuration = configuration;

        public string From(string password)
        {
            using var hmac = new HMACSHA256(
                Encoding.UTF8.GetBytes(
                    _configuration["Authentication:SecretKey"]!
                )
            );

            byte[] hashBytes = hmac.ComputeHash(
                Encoding.UTF8.GetBytes(password)
            );

            return Convert.ToBase64String(hashBytes);
        }
    }
}
