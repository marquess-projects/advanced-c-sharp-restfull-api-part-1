﻿namespace Main.Core.Data
{
    public interface IUnitOfWork
    {
        Task<bool> Commit();
    }
}
