using Main.Core.DomainObjects;
using Main.Domain.Entities.BrandEntity;

namespace Main.Domain.Entities.ModelEntity
{
    public sealed class Model : Entity, IHasEnable
    {
        public string Name { get; private set; }
        public Brand Brand { get; private set; } = null!;
        public Guid BrandId { get; private set; }
        public bool Enable { get; private set; }

        public Model(string name, Guid brandId)
        {
            Name = name;
            BrandId = brandId;
            Enable = true;

            Validate();
        }

        public Model(string name, Brand brand)
        {
            Name = name;
            Brand = brand;
            Enable = true;

            Validate();
        }

        public void Update(
            string name,
            Guid brandId,
            bool enable=true)
        {
            Name = name;
            BrandId = brandId;
            Enable = enable;

            Validate();
        }

        private void Validate()
        {
            AssertionConcern.AssertArgumentNotEmpty(
                Name,
                "Name must not be empty."
            );
        }
    }
}