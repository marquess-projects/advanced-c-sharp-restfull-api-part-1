using Main.Core.DomainObjects;

namespace Main.Domain.Entities.BrandEntity
{
    public sealed class Brand : Entity
    {
        public string Name { get; private set; }

        public Brand(string name)
        {
            Name = name;

            Validate();
        }

        public void Update(string name)
        {
            Name = name;

            Validate();
        }

        private void Validate()
        {
            AssertionConcern.AssertArgumentNotEmpty(
                Name,
                "Name must not be empty."
            );
        }
    }
}