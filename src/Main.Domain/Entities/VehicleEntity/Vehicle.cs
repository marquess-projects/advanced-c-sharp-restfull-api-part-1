using Main.Core.DomainObjects;
using Main.Domain.Entities.ModelEntity;

namespace Main.Domain.Entities.VehicleEntity
{
    public sealed class Vehicle : Entity, IAggregateRoot
    {
        public string Name { get; private set; }
        public Model Model { get; private set; } = null!;
        public Guid ModelId { get; private set; }

        public Vehicle(
            string name,
            Guid modelId)
        {
            Name = name;

            ModelId = modelId;

            Validate();
        }

        public Vehicle(
            string name,
            Model model)
        {
            Name = name;

            Model = model;

            Validate();
        }

        public void Update(
            string name,
            Guid modelId)
        {
            Name = name;

            ModelId = modelId;

            Validate();
        }

        private void Validate()
        {
            AssertionConcern.AssertArgumentNotEmpty(
                Name,
                "Name must not be empty."
            );
        }
    }
}