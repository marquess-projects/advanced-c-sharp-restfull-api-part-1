using Main.Core.DomainObjects;

namespace Main.Domain.Entities.UserEntity
{
    public enum Clearance 
    { 
        Admin = 0, 
        ViewOnly = 1, 
    }

    public class User : Entity
    {
        public string Name { get; private set; }
        public string Username { get; private set; }
        public string Password { get; private set; }
        public Clearance Clearance { get; private set; } = Clearance.ViewOnly;
    
        public User(
            string name, 
            string username, 
            string password, 
            Clearance clearance) 
        {
            Name = name;
            Username = username;
            Password = password;
            Clearance = clearance;

            Validate();
        }

        public void Update(
            string name, 
            string password, 
            Clearance clearance) 
        {
            Name = name;
            Password = password;
            Clearance = clearance;

            Validate();
        }

        private void Validate()
        {
            AssertionConcern.AssertArgumentNotEmpty(
                Name,
                "Name must not be empty."
            );

            AssertionConcern.AssertArgumentNotEmpty(
                Username,
                "Name must not be empty."
            );

            AssertionConcern.AssertArgumentNotEmpty(
                Password,
                "Name must not be empty."
            );
        }
    }
}