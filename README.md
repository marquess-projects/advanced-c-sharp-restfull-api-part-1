# About

Simple back-end web API example for a vehicle dealership system. A user with 
standard privileges can only view the brands, models, and vehicles available,
while an admin can create, delete, update, and view those. The advanced features
are:

Part 1 - Pagination and filter, Clean Architecture, CQRS+MediatR, Repository
Pattern, Fluent validation/API, Extensions, Multiple Contexts, EF core,
Automapper, Rich Model, UOW, AggregateRoot, JWT with policies and more.

Part 2 - Messaging in RabbitMQ with MassTransit, Events with INotification and
EventStore database, Write database with Postgres, Read Database with MongoDB,
docker, CI/CD and more.

The project was developed in January 2024 by Pablo S. Marques under the AGPL-3.0 license.

Project Link: https://gitlab.com/marquess-projects/web-api

# Database Connection

You shall modify the 'Main.Presentation/appsettings.Development.json' file accordingly:
```
"ConnectionStrings": {
"MainDatabaseConnection": "Host=<host>;Port=<port>;Database=<database>;Username=<username>;Password=<password>;"
}
```

# Fixing Migrations Problems

In 'Main.Presentation/program.cs', there is a procedure that deletes the current
project database, creates it again, and performs the migration automatically. But
if necessary, you can manually intervene:  

If changes were made and migration still included, an update should be enough:  
`dotnet ef --startup-project ../Main.Presentation/ database update --context VehicleContext`  
`dotnet ef --startup-project ../Main.Presentation/ database update --context UserContext`  

But you can also manually delete the migration folder and recreate it using:  
`dotnet ef --startup-project ../../Main.Presentation/ migrations add InitialMigration -o Migrations/Vehicle --context VehicleContext`  
`dotnet ef --startup-project ../../Main.Presentation/ migrations add InitialMigration -o Migrations/User --context UserContext`  
OBS: then run the database update command again.